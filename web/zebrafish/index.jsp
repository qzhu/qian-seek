<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" href="favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SEEK: Search based exploration of expression kompendia</title>
<link type="text/css" href="../css/viewer.css" rel="Stylesheet">
<link type="text/css" href="../css/jquery.qtip.css" rel="Stylesheet" />
<link type="text/css" href="../css/smoothness/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.22.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/jquery.qtip.js"></script>
<script src="../js/jquery.ba-throttle-debounce.min.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-48374664-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">

var queryNames = [];
var sessionID = "";
var organism = "zebrafish";

$(document).ready(function(){
	$.ajaxSetup({ cache: false }); 

	sessionStorage.clear();
	sessionStorage.setItem("search_alg", "RBP");
	sessionStorage.setItem("search_distance", "ZscoreHubbinessCorrected");
	sessionStorage.setItem("rbp_p", "0.99");
	sessionStorage.setItem("percent_query", "0.5");
	sessionStorage.setItem("percent_genome", "0.5");
	sessionStorage.setItem("pval_filter", "-1");
	<%@include file="../js/search.js"%>	
});

</script>
</head>
<body bgcolor="#C8C8C8" style="padding:0px;margin:0px;">
<div id="wrapper" style="margin-left:auto; margin-right:auto; width:984px; padding:0; border:0;">
<div style="height:100px;"></div>

<!-- Dark Background -->
<!-- <div style="background-color:#c8c8c8;border:1px solid black;"> -->

<!-- Light Background -->
<div style="border:1px solid #666666;">

<div style="padding-left:0px;padding-top:0px;height:60px;background-color:rgb(51,51,51);">
<table style="font:12px Arial; color:white; border:0px;">
<tr>
<td>
<img src="../img/seek_zebrafish.png" height="58" width="137" />
</td>
<td style="width:720px;"></td>
<td style="width:200px;color:white;text-align:right;" class="basic_font" id="home_panel">
<!--About | -->
<a class="white" href="index.jsp">Home</a> | 
Organism <select name="sel_organism" id="sel_organism" style="font-size:12px;"></select> 
</td>

<!-- <td style="color:red;">About | </td><td style="color:red;">Tutorial | </td><td style="color:red;">Home</td> -->

</tr>
</table>
</div>
<div style="padding-left:40px;padding-bottom:10px;height:20px;background-color:rgb(51,51,51);">
<span style="font-family:Arial;font-size:14px;color:white;">Search-based Exploration of Expression Kompendium</span>
<span style="font-family:Arial;font-size:14px;color:white;"> [Zebrafish] </span>
</div>


<div style="height:50px;"></div>

<%@include file="query_box.jsp"%>

<div style="height:50px;"></div>

</div>

<!-- Dark Background -->
<!--<div style="padding-left:150px; padding-top:20px; height:20px;font-family:Arial; font-size:14px; color:white;">-->
<!-- Light Background -->

<div style="padding-left:150px; padding-top:10px; padding-bottom:20px; height:20px;font-family:Arial; font-size:14px; color:black;">

<!--Search of co-expressions in 2800 microarray datasets, 21000 genes, across 30 platforms in Zebrafish.
-->
</div>

<!-- Dark Background -->
<!--<div style="padding-left:340px; padding-top:10px; height:20px;font-family:Arial; font-size:14px; color:white;">-->
<!-- Light Background -->

<table style="font-family:Arial; font-size:14px; color:#2b2b2b;">
<tr>
<td style="line-height:20px; padding: 10px 10px 20px 10px;">
<span style="font-size:20px;">What is SEEK?</span>
<p>
<div style="background-color:#dcdcdc; color: #2b2b2b; padding: 10px 10px 10px 10px; border-radius:10px; -moz-border-radius:10px;">
SEEK is a computational gene co-expression search engine. 
It utilizes a vast gene expression compendium 
and delivers fast, integrative, cross-platform co-expression analyses. In addition, SEEK provides instant visualization
of the co-expressed genes in relevant datasets.
</div>
</td>
<td style="width:60px;">
</td>
<td style="width:300px; line-height:20px; padding: 10px 10px 20px 10px;">
<span style="font-size:20px;">SEEK's compendium</span>
<p>
<div style="background-color:#dcdcdc; color: #2b2b2b; padding: 10px 10px 10px 10px; border-radius:10px; -moz-border-radius:10px;">
<a href="zebrafish_genes.xls"><b>19602</b></a> genes in zebrafish <br>
<a href="zebrafish_gse.xls"><b>95</b></a> mRNA microarray datasets <br>
<a href="zebrafish_platform.xls"><b>3</b></a> platforms <br>
</div>
</td>

</tr>
<tr>
<td style="line-height:20px; padding: 10px 10px 20px 10px;">
<span style="font-size:20px;">What is a query?</span>
<p>
<div style="background-color:#dcdcdc; color: #2b2b2b; padding: 10px 10px 10px 10px; border-radius:10px; -moz-border-radius:10px;">
A query is a gene or a gene-set representing a biological theme that a user wants to investigate.
Examples of a good query include genes from a common transcriptionally regulated pathway or process, a cellular
component, a molecular complex, or from a differentially expressed gene list or biomarker list.
</div>
</td>
<td style="width:60px;">
</td>
<td style="width:300px; line-height:20px; padding: 10px 10px 20px 10px;">
<span style="font-size:20px;">Using SEEK</span>
<p>
<div style="background-color:#dcdcdc; color: #2b2b2b; padding: 10px 10px 10px 10px; border-radius:10px; -moz-border-radius:10px;">
<a href="/human/SEEK_manual.pdf"><b>SEEK usage manual</b></a><br>

<!--<a href="/"><b>Frequently asked questions</b></a>-->
</td>
</div>
</tr>

<tr>
<td style="line-height:20px; padding: 10px 10px 20px 10px;">
<span style="font-size:20px;">How to begin?</span>
<p>
<div style="background-color:#dcdcdc; color: #2b2b2b; padding: 10px 10px 10px 10px; border-radius:10px; -moz-border-radius:10px;">
Start by entering a query, like <i>PTCH2 PTCH1 BOC</i>, into the search box. (Please separate genes by a single space, and
use only gene symbols.)
Then click <i>Search</i>.
</div>
</td>

<td style="width:60px;">
</td>
<td style="width:300px; line-height:20px; font-size:12px; color:#3f3f3f; padding: 10px 10px 20px 10px;">
Brought to you by the
Functional Genomics Laboratory at Princeton University, 2012
</td>
</tr>

</table>

<div style="padding-left:340px; padding-top:10px; height:20px;font-family:Arial; font-size:14px; color:black;">
</div>


<div id="statusdiv" style="display:none;">
    <div id="status_message" style="display:none;">
        <font class="basic_font"><b>
        <span id="status_percentage"></span>
        <span id="status_text"></span>
        <img src="/ajax-loader.gif" style="vertical-align:text-bottom;">
        </b></font>
    </div>

    <div id="status_pane" style="height:500px; width:300px; overflow:auto;">
    <span id="dataset_result"></span>
    <br>
    <span id="search_result"></span>
    <br>
    </div>

    <a id="search_box" href="#"></a>
</div>


</div>
<script src="../js/ready_select_organism.js"></script>
</body>
</html>
