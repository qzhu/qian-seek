package seek;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.Network;
import seek.ReadPacket;
import seek.SeekFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;


public abstract class SeekImage{

    int maxCharHeight = 15;
    int minFontSize = 15;
	SeekFile sf;

	//light background
    Color fg = Color.black;
    Color bg = Color.white;
	int neutral_red = 255;
	int neutral_green = 255;
	int neutral_blue = 229;

	//dark background
	/*
    Color bg = Color.black;
    Color fg = Color.white;
	int neutral_red = 0;
	int neutral_green = 0;
	int neutral_blue = 0;
	*/

    /*Color red = Color.red;
    Color white = Color.white;
    Color green = Color.green;
    Color black = Color.black;
	*/

	//Need Set Color
	//white bg (blue - light yellow - red)
	int[] neg_red;
	int[] neg_green;
	int[] neg_blue;
	int[] pos_red;
	int[] pos_green;
	int[] pos_blue;

	//black bg
	/*int[] neg_red =   {0, 0, 0, 0, 0, 0, 0, 0, 0};
	int[] neg_green =   {0, 10, 35, 50, 65, 80, 95, 110, 135};
	int[] neg_blue =  {21, 41, 63, 84, 105, 126, 147, 168, 189};
	int[] pos_red =   {10, 29, 57, 70, 103, 131, 169, 227, 255};
	int[] pos_green = {7, 20, 43, 60, 90, 122, 150, 188, 211};
	int[] pos_blue =  {0, 0, 0, 0, 0, 0, 0, 0, 0};
	*/
	//combined colors (neg with pos)
	int[] red;
	int[] green;
	int[] blue;

	//alternative color scheme
	/*
	int[] pos_red = {247, 229, 204, 153, 102, 65, 35, 0, 0};
	int[] pos_green = {252, 245, 236, 216, 194, 174, 139, 109, 68};
	int[] pos_blue = {253, 249, 230, 201, 164, 118, 69, 44, 27};
	int[] neg_red = {247, 224, 191, 158, 140, 140, 136, 129, 77};
	int[] neg_green = {252, 236, 211, 188, 150, 107, 65, 15, 0};
	int[] neg_blue = {253, 244, 230, 218, 198, 177, 157, 124, 75};
	*/

	int[] size; //number of conditions per dataset

    int numDatasetToShow;
    int numGeneToShow;
    int windowWidth;
    int windowHeight;
    String[] names;
    String[] description;
    Vector<String> dsets;
    Vector<String> genes;
	Vector<Float> gene_score;
	Vector<Float> dataset_score;
  	Vector<Float> pval_score;

    Map<String, String> map_genes;
    Map<String, String> map_datasets;
	Map<String, String> map_gene_symbol_entrez;

	String dset_order_file;   
 
    //final static int horizontalDatasetSpacing = 2;
    //int horizontalDatasetSpacing = 10;    
    int horizontalDatasetSpacing = 20;    
  	int horizontalUnitWidth = 4;

	int verticalGeneSpacing = 0;
    //final static int geneNameWidth = 0;
    int verticalUnitHeight = 15;

	public SeekImage(String tempDir, String organism, String choice)throws IOException{
		this.sf = new SeekFile(tempDir, organism);
		if(choice==null || !(choice.equals("blue-yellow-red") ||
		choice.equals("green-white-red") || choice.equals("blue-white-red") || 
		choice.equals("blue-black-yellow") || choice.equals("green-black-red"))){
			choice = "blue-yellow-red";
		}
		InitColors(choice);
		PrepareColors();
	}

	private void InitColors(String scale){

		if(scale.equals("blue-yellow-red")){
			neg_red = new int[]{255, 237, 199, 127, 65, 29, 34, 37, 8};
			neg_green = new int[]{255, 248, 233, 205, 182, 145, 94, 52, 29};
			neg_blue = new int[]{217, 217, 180, 187, 196, 192, 168, 148, 88};
			pos_red = new int[]{255, 255, 254, 254, 254, 236, 204, 153, 102};
			pos_green = new int[]{255, 247, 227, 196, 153, 112, 76, 52, 37};
			pos_blue = new int[]{229, 188, 145, 79, 41, 20, 2, 4, 6};
		}else if(scale.equals("green-white-red")){
			neg_red = new int[]{247, 229, 199, 161, 116, 65, 35, 0, 0};
			neg_green = new int[]{252, 245, 233, 217, 196, 171, 139, 109, 68};
			neg_blue = new int[]{245, 224, 192, 155, 118, 93, 69, 44, 27};
			pos_red = new int[]{255, 254, 252, 252, 251, 239, 203, 165, 103};
			pos_green = new int[]{245, 224, 187, 146, 106, 59, 24, 15, 0};
			pos_blue = new int[]{240, 210, 161, 114, 74, 44, 29, 21, 13};
		}else if(scale.equals("green-black-red")){
			neg_red = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
			neg_green = new int[]{0, 20, 45, 60, 85, 100, 120, 140, 159};
			neg_blue = new int[]{0, 12, 26, 40, 60, 70, 85, 95, 107};
			pos_red = new int[]{0, 20, 50, 80, 100, 120, 156, 176, 196};
			pos_green = new int[]{0, 0, 0, 0, 1, 1, 1, 2, 2};
			pos_blue = new int[]{0, 4, 10, 16, 22, 28, 34, 40, 51};
		}else if(scale.equals("blue-white-red")){
			neg_red = new int[]{247, 222, 198, 158, 107, 66, 33, 8, 8};
			neg_green = new int[]{251, 235, 219, 202, 174, 146, 113, 81, 48};
			neg_blue = new int[]{255, 247, 239, 225, 214, 198, 181, 156, 107};
			pos_red = new int[]{255, 254, 252, 252, 251, 239, 203, 165, 103};
			pos_green = new int[]{245, 224, 187, 146, 106, 59, 24, 15, 0};
			pos_blue = new int[]{240, 210, 161, 114, 74, 44, 29, 21, 13};
		}else if(scale.equals("blue-black-yellow")){
			neg_red = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
			neg_green = new int[]{0, 10, 35, 50, 65, 80, 95, 110, 135};
			neg_blue = new int[]{21, 41, 63, 84, 105, 126, 147, 168, 189};
			pos_red = new int[]{10, 29, 57, 70, 103, 131, 169, 227, 255};
			pos_green = new int[]{7, 20, 43, 60, 90, 122, 150, 188, 211};
			pos_blue = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		}
		neutral_red = pos_red[0];
		neutral_green = pos_green[0];
		neutral_blue = pos_blue[0]; 
	}

	private void PrepareColors(){
		int size = pos_red.length + 1 + neg_red.length;
		red = new int[size];
		green = new int[size];
		blue = new int[size];

		red[pos_red.length] = neutral_red;
		green[pos_red.length] = neutral_green;
		blue[pos_red.length] = neutral_blue;

		for(int i=0; i<pos_red.length; i++){
			red[pos_red.length+i+1] = pos_red[i];
			green[pos_red.length+i+1] = pos_green[i];
			blue[pos_red.length+i+1] = pos_blue[i];
		}

		for(int i=0; i<neg_red.length; i++){
			red[i] = neg_red[neg_red.length-1-i];
			green[i] = neg_green[neg_red.length-1-i];
			blue[i] = neg_blue[neg_red.length-1-i];
		}
	}


	public Color[] GetColorGradient(){
		Color[] c = null;
		c = new Color[red.length];
		for(int i=0; i<red.length; i++){
			c[i] = new Color(red[i], green[i], blue[i]);
		}
		return c;
	}

	public Color GetNeutralColor(){
		return new Color(neutral_red, neutral_green, neutral_blue);
	}

    private int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }

	public int GetVerticalUnitHeight(){
		return verticalUnitHeight;
	}

	public int GetHorizontalUnitWidth(){
		return horizontalUnitWidth;
	}

	public int[] GetDatasetSize(){
		return size;
	}
    
	public int GetHorizontalDatasetSpacing(){
		return horizontalDatasetSpacing;
	}

	public int GetVerticalGeneSpacing(){
		return verticalGeneSpacing;
	}

    public int getWindowWidth(){
    	return windowWidth;
    }
    
    public int getWindowHeight(){
    	return windowHeight;
    }

	public String[] getGeneNames(){
		return names;
	}
 
    /*public String getStem(String s){
 		if(s.indexOf("GSE")==0){
		   	int i = s.indexOf("_");
    		int j = s.indexOf(".");
    		if(i!=-1){
    			return s.substring(0, i);
    		}
    		return s.substring(0, j);
		}
		return s;
    }*/
    
    public Vector<String> getDatasets(){
    	return dsets;
    }
    
    public Vector<String> getGenes(){
    	return genes;
    }
   
	public Vector<Float> getGeneScore(){
		return gene_score;
	}

	public Vector<Float> getDatasetScore(){
		return dataset_score;
	}

	public Vector<Float> getPValueScore(){
		return pval_score;
	}

    public byte[] intToByteArray(int value) {
    	ByteBuffer buf = ByteBuffer.allocate(10);
    	buf.order(ByteOrder.LITTLE_ENDIAN);
    	buf.putInt(value);
    	return new byte[] {
    		buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
    }

	public boolean ReadMap(boolean displayDatasetKeywords) throws IOException{
    	Scanner sc = null;

		String gm = this.sf.gene_entrez_map_file;
    	map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}

		String dm = this.sf.dataset_description_file;
		if(!displayDatasetKeywords){
			dm = this.sf.dataset_title_file;
		}

    	map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}
		return true;
	}

	protected abstract void calculateSize();
	protected abstract int calculateUnitWidth(int s);
	protected abstract void paint();
	
}


