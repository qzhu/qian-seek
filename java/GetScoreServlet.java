package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import seek.Pair;
import seek.ReadScore;

public class GetScoreServlet extends HttpServlet {

	static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
			//p.waitFor();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

  public Vector<Float> getQvalue(float[] dw_pval, Vector<Pair> vp, 
  File tempDir, String sessionID) throws IOException{
		Vector<Pair> dwp = new Vector<Pair>();
		for(int i=0; i<vp.size(); i++){
			if(vp.get(i).val==0f) continue;
			Pair pp = new Pair(vp.get(i).term, 
				dw_pval[vp.get(i).index], vp.get(i).index);
			dwp.add(pp);
		}
		Collections.sort(dwp);
		String st = "/tmp/" + sessionID + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<dwp.size(); i++){
				Pair pi = dwp.get(i);
				out1.println(pi.val);
			}
		}catch(Exception e){
			System.out.println("BAD IO 1");
			return null;
		}finally{
			if(out1 !=null) out1.close();
		}
		String sv = "/tmp/" + sessionID + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				//return null;
			}
		}catch(Exception e){
			System.out.println("exception happened");
			e.printStackTrace();
		}
		Vector<Float> qval = new Vector<Float>();
		qval.setSize(vp.size());
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			int i = 0;
			while((s=in.readLine())!=null){
				qval.set(dwp.get(i).index, Float.parseFloat(s));
				i++;
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			for(int i=0; i<vp.size(); i++){
				qval.set(i, 0f);
			}
			//return null;
		}finally{
			if(in!=null) in.close();
		}
		return qval;
  }
	
  public void doGet( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException {
    // get the web applications temporary directory
    File tempDir = (File) getServletContext().
        getAttribute( "javax.servlet.context.tempdir" );
        
    System.out.println("Called");
    String sessionID = req.getParameter("sessionID");
	String organism = req.getParameter("organism");
	SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
    String request_type = req.getParameter("type");
    String ext = null;
    String name_mapping = null;
    String name_mapping_2 = null;

	String page = req.getParameter("page"); //starts from 0
	String entry_per_page = req.getParameter("per_page"); //20, 50, 100

	String query = req.getParameter("query"); //...|...|...

    Map<String, Integer> mm;    

    if(request_type.equals("dataset_weight")){
    	ext = "_dweight";
		name_mapping = sf.dataset_platform_file;
    	mm = ReadScore.readDatasetMapping(name_mapping);
    }else{
    	ext = "_gscore";
    	name_mapping = sf.gene_map_file;
    	name_mapping_2 = sf.gene_entrez_map_file;
    	mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
    }
        
    String[] keywords = StringUtils.split(req.getParameter("keyword"), "+");
	boolean show_all_dset = false;
	if(req.getParameter("show_all_dset")==null){
	}else{
	    show_all_dset = Boolean.parseBoolean(req.getParameter("show_all_dset"));
    }

	File tempFile = new File(sf.getPath(sessionID + ext));
	
	float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
	float[] sc_pval = null;
	File test_file = new File(sf.getPath(sessionID+"_pval"));
	if(test_file.exists()){
		sc_pval = ReadScore.ReadScoreBinary(sf.getPath(sessionID+"_pval"));
	}

	float[] dw_pval = null;
	File dset_test_file = new File(sf.getPath(sessionID+"_dset_pval"));
	if(dset_test_file.exists()){
		dw_pval = ReadScore.ReadScoreBinary(sf.getPath(sessionID+"_dset_pval"));
	}

	//System.out.println("keyword is " + keywords[0] + " " + mm.get(keywords[0]));
	String responseString = "";

	boolean negative_cor = ReadScore.isNegativeCor(sf.leading_path, sessionID);

	if(keywords[0].indexOf("sorted")!=-1){

		Vector<Pair> vp = null;
		if(ext.equals("_gscore")){
			vp = ReadScore.SortScores(mm, sc, negative_cor);
		}else{
			vp = ReadScore.SortScores(mm, sc, false);
		}
		int topX = -1;
		if(keywords[0].equals("all_sorted")){
			topX = vp.size();
		}else{
			int underInd = keywords[0].indexOf("_");
			topX = Integer.parseInt(keywords[0].substring(0, underInd));
		}

		String[] queries = null;
		Set<String> queries_set = null;
		if(query!=null){
			queries = StringUtils.split(query, "|");
			queries_set = new HashSet<String>();
			for(String q : queries){
				queries_set.add(q);
			}
		}

		Vector<String> annot = new Vector<String>();
		Vector<String> valid = new Vector<String>();

		if(request_type.equals("dataset_weight")){
			//dset pvalue
			Vector<Float> qvalues = null;
			if(dw_pval!=null){
				qvalues = getQvalue(dw_pval, vp, tempDir, sessionID);
			}

			for(int i=0; i<topX; i++){
				if(!show_all_dset){
					if(vp.get(i).val==0f) break;
				}
				String term = vp.get(i).term;
				String GSE_ID = ReadScore.getDatasetID(term);

				//System.out.println("gse id is " + GSE_ID);
				/*if(term.indexOf("GSE")==0){
					int dotIndex = term.indexOf(".");
					if(term.indexOf("_")!=-1){
						dotIndex = term.indexOf("_");
					}
					GSE_ID = term.substring(0, dotIndex);
				}else{
					GSE_ID = term;
				}*/
				String gse_file = sf.dataset_description_dir + "/" + GSE_ID;
				try{
					BufferedReader in = new BufferedReader(new FileReader(gse_file));
					in.readLine();
					String s = in.readLine();
					annot.add(s);
					in.close();
				}catch(IOException e){
					System.out.println("Error! " + GSE_ID + " does not exist!");
				}
				//System.out.println(vp.get(i).val + " " + vp.get(i).term);
			}

			//valid.add("Rank\tDataset\tWeight\tDescription");
			valid.add("Rank\tDataset\tCoexpression.Score\tCoexpression.PValue\tDescription");
			for(int i=0; i<topX; i++){
				if(!show_all_dset){
					if(vp.get(i).val==0f) break;
				}
				float pval = 0f;
				float qval = 0f;
				if(dw_pval!=null){
					pval = dw_pval[vp.get(i).index];
					qval = qvalues.get(vp.get(i).index);
				}
				//display only user's datasets
				if(query!=null && !queries_set.contains(vp.get(i).term)) continue;
				//valid.add(String.format("%d\t%s\t%.6f\t%s", 
				//	i+1, vp.get(i).term, vp.get(i).val, annot.get(i)));

				//valid.add(String.format("%d\t%s\t%.6f\t%.3e\t%.3e\t%s", 
				//	i+1, vp.get(i).term, vp.get(i).val, pval, qval, annot.get(i)));
				valid.add(String.format("%d\t%s\t%.6f\t%.3e\t%s", 
					i+1, vp.get(i).term, vp.get(i).val, pval, annot.get(i)));
			}

		}else{
			Map<String, String> msym2ent = 
				ReadScore.readGeneSymbol2EntrezMapping(sf.gene_entrez_map_file);
			String entrez_description = sf.entrez_description_short_file;
			Map<String, String> gene_description = new HashMap<String, String>();

			try{
				BufferedReader in = new BufferedReader(new FileReader(entrez_description));
				String s=null;
				while((s=in.readLine())!=null){
					StringTokenizer st = new StringTokenizer(s, "\t");
					gene_description.put(st.nextToken(), st.nextToken());
				}
				in.close();
			}catch(IOException e){
				System.out.println("Error! Gene description file does not exist!");
			}
				
			for(int i=0; i<topX; i++){
				String term = vp.get(i).term;
				String gene_ID = msym2ent.get(term);
				annot.add(gene_description.get(gene_ID));
			}

			valid.add("Rank\tGene\tEntrez ID\tCoexpression Score\tP-Value\tDescription");
			for(int i=0; i<topX; i++){
				float pval = 0f;
				if(sc_pval!=null){
					pval = sc_pval[vp.get(i).index];
				}

				//display only user's genes
				if(query!=null && !queries_set.contains(vp.get(i).term)) continue;

				valid.add(String.format("%d\t%s\t%s\t%.4f\t%.4f\t%s", 
					i+1, vp.get(i).term, msym2ent.get(vp.get(i).term), 
					vp.get(i).val, pval, annot.get(i)));
			}

		}

		int page_id = -1;

		if(page!=null){
			page_id = Integer.parseInt(page);
			int max_per_page = Integer.parseInt(entry_per_page);
			int page_begin = page_id * max_per_page;
			int page_end = max_per_page + page_begin;
			Vector<String> ns = new Vector<String>();
			ns.add(valid.get(0));
			for(int i=page_begin+1; i<page_end+1 && i<valid.size(); i++){
				ns.add(valid.get(i));
			}
			responseString = StringUtils.join(ns, "\n");
		}else{
			responseString = StringUtils.join(valid, "\n");
		}

		/*
		String[] stbuilder = new String[valid.size()];
		for(int i=0; i<valid.size(); i++){
			stbuilder[i] = valid.get(i);
		}
		responseString = StringUtils.join(stbuilder, "\n");
		*/
	}else{

		float totWeight = 0;	
		for(int i=0; i<sc.length; i++){
			totWeight+=sc[i];
		}

		String[] stbuilder = new String[keywords.length];
		for(int i=0; i<stbuilder.length; i++){
			stbuilder[i] = String.format("%.1f", sc[mm.get(keywords[i])]/totWeight * 100.0);
		}

		responseString = StringUtils.join(stbuilder, ";");

	}
	
	
	res.setContentType("text/plain");
	res.setCharacterEncoding("UTF-8");

	res.getWriter().write(responseString);

  }

}
