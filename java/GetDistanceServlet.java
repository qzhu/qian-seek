package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import seek.Pair;
import seek.ReadScore;
import seek.CompareList;

public class GetDistanceServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{

		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");

		String organism1 = req.getParameter("organism1");
		String organism2 = req.getParameter("organism2");

		String sessionID1 = req.getParameter("sessionID1");
		String sessionID2 = req.getParameter("sessionID2");

		String show_all = req.getParameter("show_all");
		String option = req.getParameter("option");
		String show_list = req.getParameter("show_list"); //shared, exclusive_to_left, or exclusive_to_right

		boolean showAll = false;
		if(show_all!=null && show_all.equals("true")){
			showAll = true;
		}

		CompareList cp = new CompareList();
		cp.Compare(organism1, organism2, sessionID1, sessionID2, tempDir.getAbsolutePath(), option, show_list, showAll);
		Vector<String> stt = cp.stt;
		String[] stbuilder = new String[stt.size()+2];
		//Query
		stbuilder[0] = StringUtils.join(cp.query1, " ");
		stbuilder[1] = StringUtils.join(cp.query2, " ");
		//Result
		for(int i=0; i<stt.size(); i++)
			stbuilder[i+2] = stt.get(i);
		String responseString = StringUtils.join(stbuilder, "\n");	
		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write(responseString);
	}

}
