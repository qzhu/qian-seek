<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Query</title>
<link type="text/css" href="jquery.qtip.min.css" rel="Stylesheet" />
<link type="text/css" href="css/smoothness/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script src="jquery-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="jquery.qtip.min.js"></script>
<script src="jquery.ba-throttle-debounce.min.js"></script>

<%@ page import="java.util.*, java.io.*, seek.Seek" %>
<%! Seek ss = null; %>
<% try{
	String sessionID = session.getId();
	File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
	ss = new Seek(request.getParameter("query_genes"), 
		request.getParameter("datasets"), tempDir.getAbsolutePath(), sessionID, false);

	Vector<String> dsets = ss.GetDatasets(); //result datasets
	Vector<String> genes = ss.GetGenes(); //result genes
	
	Vector<String> query = (Vector<String>) session.getAttribute("query_attr");
	int d_page = 1;
	int g_page = 1;
	int d_num_per_page = 10;
	int g_num_per_page = 100;
%>

<script>
$(document).ready(function(){
var sessionID = "<%=session.getId()%>";
var queryNames = [];
var geneNames = [];
var dsetNames = [];
<%
	int i;
	for(i=0; i<dsets.size(); i++){
		%> dsetNames.push("<%=dsets.get(i)%>.bin");
	    <%
	}

	for(i=0; i<genes.size(); i++){
		%> geneNames.push("<%=genes.get(i)%>"); 
        <%
	}
	
	for(i=0; i<query.size(); i++){
		%> queryNames.push("<%=query.get(i)%>");
		<%
	}
%>
	sessionStorage.clear();
	//sessionStorage.setItem("gene_refine", "true"); //genes from results only (also only top X is shown)
	//sessionStorage.setItem("dataset_refine", "true"); //datasets from results only (not whole list) (also only top X is shown)
	sessionStorage.setItem("gene_refine_list", "");
	sessionStorage.setItem("dataset_refine_list", "");
	
	sessionStorage.setItem("gene_refine_list", queryNames.join(";")); //query by default is still in query

});

</script>


</head>

<body id="main_body">



<a href="viewer.jsp?d_page=<%=d_page%>&g_page=<%=g_page%>&d_num_per_page=<%=d_num_per_page%>&g_num_per_page=<%=g_num_per_page%>">Visualize</a>
<br>
<div id="result_list" style="display:none;">
<%
	for(i=0; i<dsets.size(); i++){
		%> <%=dsets.get(i)%><br>
	    <%
	}

	for(i=0; i<genes.size(); i++){
		%> <%=genes.get(i)%><br> 
        <%
	}

	}catch(Exception e){
		%>Bad page!<%
	}

%>
</div>

</body>
</html>
