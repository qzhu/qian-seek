package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.awt.Color;

public class GetDescription extends HttpServlet {

	public Map<String,String> ReadMap(String file) throws IOException{
		Map<String,String> m = new HashMap<String,String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String ss = null;
			while((ss=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(ss, "\t");
				String s1 = st.nextToken();
				String s2 = st.nextToken();
				m.put(s1, s2);
			}
			in.close();
		}catch(IOException e){
			System.out.println("BAD I/O!");
		}
		return m;
	}

  public void doGet( HttpServletRequest req, HttpServletResponse res )
  throws ServletException, IOException {

	File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
	String dir = tempDir.getAbsolutePath();
	String organism = req.getParameter("organism");
	SeekFile sf = new SeekFile(dir, organism);
	String parsedGSE = sf.dataset_description_dir;
	String parsedEntrez = sf.entrez_description_dir;
	String parsedGSM = sf.sample_description_dir;

	String shortEntrez = sf.entrez_description_short_file;
	String shortGSE = sf.dataset_description_file;
	String shortGSETitle = sf.dataset_title_file;
	String pubmedFile = sf.dataset_pubmed_file;

	//accepted parameters: dset (array), gene (array), geneLong, dsetLong, needSample
	boolean askDset = false;
	boolean askGene = false;
	boolean askSample = false;
	boolean dsetLong = false;
	boolean geneLong = false;

	if(req.getParameter("dset")!=null)
		askDset = true;
	if(req.getParameter("gene")!=null)
		askGene = true;

	if(req.getParameter("geneLong")!=null &&
		req.getParameter("geneLong").equals("true"))
		geneLong = true;

	if(req.getParameter("dsetLong")!=null &&
		req.getParameter("dsetLong").equals("true"))
		dsetLong = true;

	if(req.getParameter("needSample")!=null &&
		askDset && askGene)
		askSample = true;

	Vector<String> dd = null;
	String dsetStr = null;
	if(askDset){
		String[] dsetArray = StringUtils.split(req.getParameter("dset"), ",");
		dd = new Vector<String>();
		if(!dsetLong){
			Map<String,String> dsetDescription = ReadMap(shortGSE);
			Map<String,String> dsetDescriptionTitle = ReadMap(shortGSETitle);
			Map<String,String> dsetPubmed = ReadMap(pubmedFile);
			for(String s: dsetArray){
				String dset_name = ReadScore.getDatasetID(s);
				/*if(s.indexOf("GSE")==0){
					dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
				}else{
					dset_name = s;
				}*/
				dd.add(dset_name + ";;" + dsetDescription.get(dset_name)+ ";;" + 
					dsetDescriptionTitle.get(dset_name) + ";;" + dsetPubmed.get(dset_name));
			}
		}else{
			for(String s : dsetArray){
				String dset_name = ReadScore.getDatasetID(s);
				/*if(s.indexOf("GSE")==0){
					dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
				}else{
					dset_name = s;
				}*/

				String dsetPath = parsedGSE + "/" + dset_name;
				try{
					BufferedReader in = new BufferedReader(new FileReader(dsetPath));
					String ss = null;
					Vector<String> ll = new Vector<String>();
					while((ss=in.readLine())!=null)
						ll.add(ss);
					dd.add(StringUtils.join(ll, ";;"));
					in.close();
				}catch(IOException e){
					System.out.println("BAD I/O!");
				}
			}
		}
		dsetStr = StringUtils.join(dd, "==");
	}

	Vector<String> gg = null;
	String geneStr = null;
	if(askGene){
		String[] geneArray = StringUtils.split(req.getParameter("gene"), ",");
		gg = new Vector<String>();
		if(!geneLong){
			Map<String,String> geneDescription = ReadMap(shortEntrez);
			for(String s: geneArray)
				gg.add(s+";;" + geneDescription.get(s));
		}else{
			for(String s : geneArray){
				String genePath = parsedEntrez + "/" + s;
				try{
					BufferedReader in = new BufferedReader(new FileReader(genePath));
					String ss = null;
					Vector<String> ll = new Vector<String>();
					while((ss=in.readLine())!=null){
						ll.add(ss);
					}
					gg.add(StringUtils.join(ll, ";;"));
					in.close();
				}catch(IOException e){
					System.out.println("BAD IO");
				}
			}
		}
		geneStr = StringUtils.join(gg, "==");
	}
	
	Vector<String> mm = null;
	String gsmStr = null;
	if(askSample){
		mm = new Vector<String>();
		String[] dsetArray = StringUtils.split(req.getParameter("dset"), ",");
		Map<String,String> dset_plat = ReadMap(sf.dataset_platform_file);
		for(int i=0; i<dsetArray.length; i++){
			String s = dsetArray[i];
			String dset_platform = dset_plat.get(s);

			String dset_name = ReadScore.getDatasetName(s);
			/*if(dset_platform.indexOf("GPL")==0){
				dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
			}else{
				dset_name = s;
			}*/
			//String dset_platform = StringUtils.split(s, ".")[1];
			//String dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
			String dsetPath = parsedGSM + "/" + dset_platform + "/" + dset_name;
			try{
				BufferedReader in = new BufferedReader(new FileReader(dsetPath));
				String ss = null;
				Vector<String> ll = new Vector<String>();
				while((ss=in.readLine())!=null){
					ll.add(ss);
				}
				in.close();
				mm.add(StringUtils.join(ll, ";;"));
			}catch(IOException e){
				mm.add("Array not found" + ";;" + "Array not found");
				System.out.println("BAD I/O");
			}
		}
		gsmStr = StringUtils.join(mm, "==");
	}

	Vector<String> response = new Vector<String>();
	if(askDset)
		response.add(dsetStr);
	if(askGene)
		response.add(geneStr);
	if(askSample)
		response.add(gsmStr);

	res.setContentType("text/plain");
	res.setCharacterEncoding("UTF-8");
	res.getWriter().write(StringUtils.join(response, "&&"));

  }

}
