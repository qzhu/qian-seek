
/* this shows the dataset description section */
function show_dset_event(){
	var tt = $("#show_datasets").find("font").find("a").text();
	$("#show_datasets").find("font").find("a").text("All dataset descriptions")
		.attr("href", "#");
	
	var yi = 0;
	var t1a = $("<table>")
			.css("font-family", "Arial")
			.css("font-size", "12px")
			.css("line-height", "150%");

	var t1 = $("<table>")
			.attr("id", "dset_content")
			.css("font-family", "Arial")
			.css("font-size", "12px")
			.css("line-height", "150%");

	var t2 = $("<tr>")
			.append($("<td>")
					.css("width", "200px")
					.css("padding-left", "20px")	
					.append($("<font>")
							.css("font-size", "20px")
							.text("Related Datasets"))
					.append("<br>")
					.append($("<font>")
							.css("font-size", "12px")
							.text("(<%=datasetBegin%>...<%=datasetEnd%>)"))
					.append($("<div>")
							.attr("id", "dataset_label_detail")
							.css("font-size", "10px")
							.css("color", "blue")
							.append($("<a>")
									.attr("href", "#dset_div")
									.text("Show details"))
							)
					);
	t1a.append(t2);
			
	for(yi=0; yi<dsetNames.length; yi++){
		var tx = $("<tr>")
				.append($("<td>").css("width", "100px"))
				.append($("<td>")
						.css("width", "800px")
						.append($("<font>")
								.css("font-size", "16px")
								.css("color", "gray")
								.append($("<b>")
										.text(roman[yi] + ". ")
										)
								)
						.append(dsetDescriptionOneLine[yi])
						);
		t1.append(tx);
	}
	
	t1a = t1a.after(t1);
			
	$("#dset_div").empty().append(t1a);

	$("#dataset_label_detail").click(function(event){
		var tt = $("#dataset_label_detail").find("a").text();
		//alert($("#dset_content").find("tr").html());
		var par = $("#dset_content").find("tr");
		//alert(jQuery("#dataset_label_detail").parent().parent().html());
		for(yi=0; yi<dsetNames.length; yi++){
			//alert(par.html());
			var two = (tt=="Hide details") ? 
				dsetDescriptionOneLine[yi] : dsetDescriptions[yi];
			var c = $("<td>").css("width", "100px")
					.after($("<td>")
						.css("width", "800px")
						.append($("<font>")
							.css("font-size", "16px")
							.css("color", "gray")
							.append($("<b>")
									.text(roman[yi] + ". ")
									)
								)
						.append(two)
						);
			par.empty().append(c);
			par = par.next();
		}
		activate_external_link();

		if(tt=="Hide details"){
			$("#dataset_label_detail").find("a").text("Show details");
		}else{
			$("#dataset_label_detail").find("a").text("Hide details");
		}
		return false;
	});
	return false;
}

function detect_gene_selection(){
	var gene_ref = sessionStorage.getItem("gene_refine_list");
	if(gene_ref==null){
		return;
	}
	var sorted_q = queryNames.sort().join(";");
	var sorted_g = gene_ref.split(";").sort().join(";");
	
	var nw;
	if(sorted_g!="" && sorted_q!=sorted_g){
		if((nw = $("#gene_selection")).length==0){
			nw = $("<i>")
					.attr("id", "gene_selection")
					.append("Custom ")
					.append($("<a>")
							.attr("href", "#")
							.text("gene selection")
							)
					.append(" detected. ")
					.append($("<a>")
							.attr("href", "#")
							.attr("title", "Delete selection")
							.append($("<img>")
								.attr("src", "../img/trash.png")
								.css("height", "10px")
								.css("width", "auto")
								)
							.qtip()
							.click(function(){
								sessionStorage.setItem("gene_refine_list", queryNames.join(";"));
								$("#selection_done").find("button").trigger("click");
								return false;
							})
							);
			if($("#refine_info").find("i").length==0){
				$("#refine_info").append(nw);
			}else{
				$("#refine_info").append("<br>").append(nw);
			}
		}
		
		var gg = gene_ref.split(";");
		var geneH = [];
		for(var vi = 0; vi<gg.length; vi++){
			geneH.push(mapID2HGNC[gg[vi]]);
		}
		nw.children("a:nth-child(1)").attr("title", geneH.join(" "))
		.qtip("destroy").qtip({
				position: {
					my: "top left",
					at: "bottom right",
					target: nw.children("a:nth-child(1)")
				},
				style: {
					width:"300px"
				},
			});
		$("#navigation #search_1").attr("href", "check_query.jsp?query_genes=" + 
			geneH.join("+"));
		$("#navigation").show();
	}else{
		if((nw = $("#gene_selection")).length!=0){
			if(nw.prev().is("br")){
				nw.prev().remove();
			}
			nw.remove();
			if($("#refine_info").children().length==0){
				$("#navigation").hide();
			}
		}
	}
}

function detect_dataset_selection(){
	var dataset_ref = sessionStorage.getItem("dataset_refine_list");
	if(dataset_ref==null){
		return;
	}
	var nw;
	if(dataset_ref!=""){
		if((nw = $("#dataset_selection")).length==0){
			nw = $("<i>")
					.attr("id", "dataset_selection")
					.append("Custom ")
					.append($("<a>")
							.attr("href", "#")
							.text("dataset selection")
							)
					.append(" detected. ")
					.append($("<a>")
							.attr("href", "#")
							.attr("title", "Delete selection")
							.append($("<img>")
								.attr("src", "../img/trash.png")
								.css("height", "10px")
								.css("width", "auto")
								)
							.qtip()
							.click(function(){
								sessionStorage.setItem("dataset_refine_list", "");
								$("#selection_done").find("button").trigger("click");
								return false;
							})
							);
			if($("#refine_info").find("i").length==0){
				$("#refine_info").append(nw);
			}else{
				$("#refine_info").append("<br>").append(nw);
			}
		}
		nw.children("a:nth-child(1)").attr("title", dataset_ref.split(";").join("<br>"))
		.qtip("destroy").qtip({ 
				id: "refine_info_dataset",
				position: {
					my: "top left",
					at: "bottom right",
					target: nw.children("a:nth-child(1)")
				},
				style: {
					width:"300px"
				},
				show:{
					event: "mouseenter", 
					solo: true, 
					effect: function(offset){$(this).slideDown(100);} //slidedown animation
				},
				hide:{
					event: "mouseleave",
					delay: 100,
					fixed: true
				}
		});
		$("#navigation").show();
	}else{
		if((nw = $("#dataset_selection")).length!=0){
			if(nw.prev().is("br")){
				nw.prev().remove();
			}
			nw.remove();
		}
		if($("#refine_info").children().length==0){
			$("#navigation").hide();
		}
	}
}

/* this shows the gene description section */
function show_gene_event(){
	var tt = $("#show_genes").find("font").find("a").text();
	$("#show_genes").find("font").find("a").text("All gene descriptions");
	$("#show_genes").find("font").find("a").attr("href", "#");
	var yi = 0;
	var t1 = $("<table>")
			.attr("id", "gene_content")
			.css("font-family", "Arial")
			.css("font-size", "12px")
			.css("line-height", "150%");

	var t1a = $("<table>")
			.css("font-family", "Arial")
			.css("font-size", "12px")
			.css("line-height", "150%");

	var t2 = $("<tr>")
			.append($("<td>")
					.css("width", "400px")
					.css("padding-left", "20px")
					.append($("<font>")
							.css("font-size", "20px")
							.text("Search Results: Gene Descriptions"))
					.append("<br>")
					.append($("<font>")
							.css("font-size", "12px")
							.text("(<%=geneBegin%>...<%=geneEnd%>)"))
					.append($("<div>")
							.attr("id", "gene_label_detail")
							.css("font-size", "10px")
							.css("color", "blue")
							.append($("<a>")
									.attr("href", "#gene_div")
									.text("Show details"))
							)
					);
	t1a.append(t2);
	
	for(yi=0; yi<geneNames.length; yi++){
		var tx = $("<tr>")
				.append($("<td>").css("width", "100px"))
				.append($("<td>")
						.css("width", "800px")
						.append(geneDescriptionOneLine[yi])
						);
		t1.append(tx);
	}

	t1a = t1a.after(t1);

	$("#gene_div").empty().append(t1a);

	$("#gene_label_detail").click(function(event){
		var tt = $("#gene_label_detail").find("a").text();
		var par = $("#gene_content").find("tr");
		//var par = $("#gene_label_detail").parent().parent();
		for(yi=0; yi<geneNames.length; yi++){
			var two = (tt=="Hide details") ? 
				geneDescriptionOneLine[yi] : geneDescriptions[yi];
			par.children("td").eq(1).html(two);
			par = par.next();
		}
		activate_external_link();

		if(tt=="Hide details"){
			$("#gene_label_detail").find("a").text("Show details");
		}else{
			$("#gene_label_detail").find("a").text("Hide details");
		}
		return false;
	});
	
	return false;
}

function show_gene_refine(){
	for(var vi=0; vi<geneNames.length; vi++){
		var gene_str = "../img/red_unchecked.png";
		if(StorageListFind("gene_refine_list", geneNames[vi])){
			gene_str = "../img/red_checked.png";
		}else{
			gene_str = "../img/red_unchecked.png";
		}
		$("#gene_"+geneNames[vi]).find("a").after(	
			$(document.createElement("img"))
				.css("height", "10px")
				.css("width", "auto")
				.css("margin-left", "6px")
				.css("margin-right", "6px")
				.attr("src", gene_str)
				.attr("id", "enable_gene_" + geneNames[vi])
				.click(function(){
					var state = $(this).attr("src");
					var gn = $(this).attr("id").split("_")[2];
					if(state=="../img/red_unchecked.png"){
						$(this).attr("src", "../img/red_checked.png");
						StorageListAdd("gene_refine_list", gn);
					}else{
						$(this).attr("src", "../img/red_unchecked.png");
						StorageListDelete("gene_refine_list", gn);
					}
					return false;	
				})
		);	
	}
	$("#gene_names").css("width", "120px");
	for(var vi=0; vi<queryNames.length; vi++){
		var gene_str = "../img/red_checked.png";
		if(StorageListFind("gene_refine_list", queryNames[vi])){
			gene_str = "../img/red_checked.png";
		}else{
			gene_str = "../img/red_unchecked.png";
		}
		$("#query_"+queryNames[vi]).find("a").after(
			$(document.createElement("img"))
				.css("height", "10px")
				.css("width", "auto")
				.css("margin-left", "6px")
				.css("margin-right", "6px")
				.attr("src", gene_str)
				.attr("id", "enable_query_" + queryNames[vi])
				.click(function(){
					var state = $(this).attr("src");
					var gn = $(this).attr("id").split("_")[2];
					if(state=="../img/red_unchecked.png"){
						$(this).attr("src", "../img/red_checked.png");
						StorageListAdd("gene_refine_list", gn);
					}else{
						$(this).attr("src", "../img/red_unchecked.png");
						StorageListDelete("gene_refine_list", gn);
					}
					return false;	
				})
		);	
	}
	$("#query_names").css("width", "120px");
}

function hide_dataset_refine(){
	for(var vi=0; vi<dsetNames.length; vi++){
		$("#enable_dataset_"+vi).prev().remove();
		$("#enable_dataset_"+vi).remove();
	}
}

function show_dataset_refine(){
	for(var vi=0; vi<dsetNames.length; vi++){
		var dset_str = "../img/red_checked.png";
		if(StorageListFind("dataset_refine_list", dsetNames[vi])){
			dset_str = "../img/red_checked.png";
		}else{
			dset_str = "../img/red_unchecked.png";
		}
		$("#dset_"+vi).find(":last").after($(document.createElement("br")));
		$("#dset_"+vi).find("br:last").after(
			$(document.createElement("img"))
				.css("height", "10px")
				.css("width", "auto")
				.css("margin-left", "0px")
				.css("margin-right", "0px")
				.attr("src", dset_str)
				.attr("id", "enable_dataset_" + vi)
				.click(function(){
					var state = $(this).attr("src");
					var dn = $(this).attr("id").split("_")[2];
					
					if(state=="../img/red_unchecked.png"){
						$(this).attr("src", "../img/red_checked.png");
						StorageListAdd("dataset_refine_list", dsetNames[dn]);
					}else{
						$(this).attr("src", "../img/red_unchecked.png");
						StorageListDelete("dataset_refine_list", dsetNames[dn]);
					}
					return false;	
				})
		);	
	}
}

