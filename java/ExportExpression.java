package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import seek.ReadPacket;
import seek.Network;
import seek.GetExpression;

public class ExportExpression extends HttpServlet {

	private Vector<Vector<String> > ReadGSM(Vector<String> dset, Map<String,String> mapDsetPlat, 
		String parsedGSM) throws IOException{

		Vector<Vector<String> > mm = new Vector<Vector<String> >();

		for(int i=0; i<dset.size(); i++){
			Vector<String> vs = new Vector<String>();
			String s = dset.get(i);
			String dset_platform = mapDsetPlat.get(s);
			String dset_name = ReadScore.getDatasetID(s);
			//String dset_platform = StringUtils.split(s, ".")[1];
			//String dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
			String dsetPath = parsedGSM + "/" + dset_platform + "/" + dset_name;
			try{
				BufferedReader in = new BufferedReader(new FileReader(dsetPath));
				String ss = null;
				while((ss=in.readLine())!=null)
					vs.add(ss);
				in.close();
			}catch(IOException e){
				System.out.println("BAD I/O");
			}
			mm.add(vs);
		}
		return mm;
	}

	private Map<String,String> ReadGeneMap(String gm) throws IOException{
    	Scanner sc = null;
    	Map<String,String>map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}
		return map_genes;
	}

	//map dataset to platform
	private Map<String,String> ReadDatasetMap(String dm) throws IOException{
		Scanner sc = null;
    	Map<String,String> map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}
		return map_datasets;
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		int i, j, k;
		int num_dataset;
		int num_gene;
		int num_query;
		int topG = 0;
		int topD = 0;
		float pval_cutoff = -1;

		//mode 0: Get Expression of Genes in Several Datasets
		//mode 1: Get Coexpression matrix of Genes in Several Datasets
		//mode 2: Get Expression of One Gene + Query in One Dataset
		int mode = Integer.parseInt(req.getParameter("mode"));
		String organism = req.getParameter("organism");

		boolean annotation = Boolean.parseBoolean(req.getParameter("annotation"));
		boolean normalize = Boolean.parseBoolean(req.getParameter("normalize"));
		boolean d_range = Boolean.parseBoolean(req.getParameter("d_range"));
		boolean g_range = Boolean.parseBoolean(req.getParameter("g_range"));

		boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
		boolean multiplyDatasetWeight = false;

		if(filter_by_pval)
			pval_cutoff = Float.parseFloat(req.getParameter("pval_cutoff"));

		if(g_range)
			topG = Integer.parseInt(req.getParameter("top_g"));
		if(d_range)
			topD = Integer.parseInt(req.getParameter("top_d"));

		String sessionID = req.getParameter("sessionID");

		Vector<String> onlyGenes = null;
		String specifyGenes = null;
		if(req.getParameter("specify_genes")!=null){
			specifyGenes = java.net.URLDecoder.decode(
				req.getParameter("specify_genes"), "UTF-8");
			String[] xs = StringUtils.split(specifyGenes);
			if(xs.length>0){
				onlyGenes = new Vector<String>();
				for(String x : xs)
					onlyGenes.add(x);
			}
		}

		File tempDir = (File) getServletContext().
			getAttribute( "javax.servlet.context.tempdir" );
		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);

		//Required only for mode 1
		float rbp_p = 0.99f;
		if(mode==1){
			rbp_p = Float.parseFloat(req.getParameter("rbp_p"));
			multiplyDatasetWeight = Boolean.parseBoolean(req.getParameter("mult_by_dset_weight"));
		}

		//sample_order is required only for mode 0, 2
		int[][] sample_order = null;
		if(mode==0 || mode==2){
			String[] ss = StringUtils.split(req.getParameter("sample_order"), "+");
			num_dataset = ss.length;
			sample_order = new int[num_dataset][];
			for(i=0; i<ss.length; i++){
				String[] s1 = StringUtils.split(ss[i], "|");
				sample_order[i] = new int[s1.length];
				for(j=0; j<s1.length; j++)
					sample_order[i][j] = Integer.parseInt(s1[j]);
			}
		}

		Vector<String> dset = new Vector<String>();
		Vector<String> gene = new Vector<String>();
		Vector<String> query = new Vector<String>();

		//Read maps
		Map<String, String> map_genes;
		Map<String, String> map_datasets; //dset to platform map
		Map<String, String> map_gene_symbol_entrez;

		float[] sc = null; //temp
		float[] sc_pval = null; //temp
		Map<String, Integer> mm = null; //temp
		Vector<Pair> vp = null; //temp

		boolean negative_cor = ReadScore.isNegativeCor(sf.leading_path, sessionID);

		String dataset_path = sf.getPath(sessionID + "_dweight");
		String pval_path = sf.getPath(sessionID + "_pval");
		String gene_path = sf.getPath(sessionID + "_gscore");
		String dset_title_path = sf.dataset_description_dir;
		String gsm_path = sf.sample_description_dir;

		String gene_map_path = sf.gene_entrez_map_file;
		String dset_map_path = sf.dataset_description_file;

		map_genes = ReadGeneMap(gene_map_path);
		map_datasets = ReadDatasetMap(dset_map_path);

		Map<String,Float> dset_score = new HashMap<String,Float>();
		float dset_sum_score = 0f;
		int compendium_num_dset = 0;

		map_gene_symbol_entrez = 
		ReadScore.readGeneSymbol2EntrezMapping(gene_map_path);
		mm = ReadScore.readDatasetMapping(sf.dataset_platform_file);
		sc = ReadScore.ReadScoreBinary(dataset_path);
		vp = ReadScore.SortScores(mm, sc, false);        

		//create dataset score map                
		for(i=0; i<vp.size(); i++){
			dset_score.put(vp.get(i).term, new Float(vp.get(i).val));
			if(vp.get(i).val>0f){	
				dset_sum_score+=vp.get(i).val;
				compendium_num_dset++;
			}
		}

		if(d_range){
			for(i=0; i<Math.min(topD, vp.size()); i++){
				float fval = (float) vp.get(i).val;
				if(fval==0.0f) continue;
				dset.add(vp.get(i).term);
			}
		}else{
			String[] dsetS;
			dsetS = StringUtils.split(req.getParameter("dataset"), "+");
			for(i=0; i<dsetS.length; i++)
				dset.add(dsetS[i]);
		}

		mm = ReadScore.readGeneMapping(sf.gene_map_file, 
		gene_map_path);
		sc = ReadScore.ReadScoreBinary(gene_path);

		File test_file = new File(pval_path);
		if(test_file.exists()){
			sc_pval = ReadScore.ReadScoreBinary(pval_path);
		}

		//filter by pval
		if(filter_by_pval && sc_pval!=null){
			for(i=0; i<sc.length; i++){
				if(sc_pval[i]>pval_cutoff)
					sc[i] = -320.0f;	
			}
		}

		//filter by user-specified genes
		if(onlyGenes!=null && onlyGenes.size()>0){
			Set<Integer> include_id = new HashSet<Integer>();
			for(i=0; i<onlyGenes.size(); i++){
				int internal_id = mm.get(onlyGenes.get(i));
				include_id.add(internal_id);
			}
			for(i=0; i<sc.length; i++){
				if(!include_id.contains(i)){
					sc[i] = -320.0f;
				}
			}
		}

		vp = ReadScore.SortScores(mm, sc, negative_cor);

		if(g_range){
			for(i=0; i<Math.min(topG, vp.size()); i++)
				gene.add(map_gene_symbol_entrez.get(vp.get(i).term));
		}else{
			String[] geneS;
			geneS = StringUtils.split(req.getParameter("gene"), "+");
			for(i=0; i<geneS.length; i++)
				gene.add(geneS[i]);
		}

		//all of them require query genes
		String[] queryS;
		queryS = StringUtils.split(req.getParameter("query"), "+");
		for(i=0; i<queryS.length; i++)
			query.add(queryS[i]);
	
		num_dataset = dset.size();
		num_gene = gene.size();
		num_query = query.size();
		
		Vector<Vector<String> > mp = ReadGSM(dset, map_datasets, gsm_path);
	
		GetExpression ge = null;
		Vector<String> rS = new Vector<String>();

		if(mode==1){ //coexpression
			ge = new GetExpression(true, normalize, dset, gene, query, rbp_p);
			ge.PerformQueryCoexpression(
				sf.getPath(sessionID+"_dweight_comp"), 
				sf.getPath(sessionID+"_query_part"), 
				sf.dataset_platform_file, sf.pclserver_port);
			float[][] qcomat = ge.GetQueryCoexpression(); //dataset, gene
			float[][] comat = ge.GetGeneCoexpression(); //dataset, gene
			//boolean multiplyDatasetWeight = true;

			rS.add("Datasets");
			for(i=0; i<num_dataset; i++){
				String dset_name = dset.get(i);
				//String dset_name = StringUtils.split(
				//	StringUtils.split(dset.get(i), ".")[0], "_")[0];
				rS.add(dset_name+": "+map_datasets.get(dset_name));
			}

			rS.add("");
			rS.add("Query Cross Validation (NP denotes Not Present)");

			for(j=0; j<num_query; j++){
				Vector<String> v1 = new Vector<String>();
				v1.add(map_genes.get(query.get(j)));
				for(i=0; i<num_dataset; i++){
					float v = qcomat[i][j];
					if(v==-9999){
						v1.add("NP");
					}else{
						v*=num_query;
						v1.add(String.format("%.1f", v));
					}
				}
				rS.add(StringUtils.join(v1, "\t"));
			}

			rS.add("");

			rS.add("Gene-Query Coexpression (NP denotes Not Present)");

			for(j=0; j<num_gene; j++){
				Vector<String> v1 = new Vector<String>();
				v1.add(map_genes.get(gene.get(j)));
				for(i=0; i<num_dataset; i++){
					float factor = dset_score.get(dset.get(i)) / dset_sum_score * compendium_num_dset;
					float v = comat[i][j];
					if(Float.isNaN(v) || Float.isInfinite(v) || v<-10.0f || v>10.0f){
						v1.add("NP");
					}else{
						if(multiplyDatasetWeight)
							v *= factor;
						v1.add(String.format("%.1f", v));
					}
				}
				rS.add(StringUtils.join(v1, "\t"));
			}

		}else if(mode==0 || mode==2){ //expression
			ge = new GetExpression(true, normalize, dset, gene, query);
			ge.PerformQuery(sf.pclserver_port);
			float[][][] qmat = ge.GetQueryExpression(); //dataset, gene, array
			float[][][] gmat = ge.GetGeneExpression(); //dataset, gene, array

			for(i=0; i<num_dataset; i++){
				rS.add("---------------------------------------------------------");
				String dset_name = dset.get(i);
				//String dset_name = StringUtils.split(
				//	StringUtils.split(dset.get(i), ".")[0], "_")[0];
				rS.add("Dataset " + i + ": " + dset_name+" "+map_datasets.get(dset_name));
				Vector<String> gsm = mp.get(i);
				for(j=0; j<gsm.size(); j++){
					rS.add(gsm.get(sample_order[i][j]));
				}

				rS.add("");
				rS.add("Query Expression (NP denotes Not Present)");

				for(j=0; j<num_query; j++){
					Vector<String> v1 = new Vector<String>();
					v1.add(map_genes.get(query.get(j)));
					for(k=0; k<gsm.size(); k++){
						float v = qmat[i][j][sample_order[i][k]];
						if(v>327.0f) v1.add("NP");
						else v1.add(String.format("%.1f", v));
					}
					rS.add(StringUtils.join(v1, "\t"));
				}

				rS.add("");
				rS.add("Gene Expression (NP denotes Not Present)");
				for(j=0; j<num_gene; j++){
					Vector<String> v1 = new Vector<String>();
					v1.add(map_genes.get(gene.get(j)));
					for(k=0; k<gsm.size(); k++){
						float v = gmat[i][j][sample_order[i][k]];
						if(v>327.0f) v1.add("NP");
						else v1.add(String.format("%.1f", v));
					}
					rS.add(StringUtils.join(v1, "\t"));
				}
				rS.add("");
			}
		}
	
		String export_file = sf.getPath(sessionID + "_export");
		PrintWriter out1 = new PrintWriter(new FileWriter(export_file));
		for(i=0; i<rS.size(); i++){
			out1.println(rS.get(i));
		}
		out1.close();

		//res.setContentType("text/plain");
		//res.setCharacterEncoding("UTF-8");
		//res.getWriter().write(StringUtils.join(rS, "\n"));	

	}
}
