package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import seek.GeneSet;
import seek.Pair;
import java.text.*;
import seek.GeneEnrichmentCore;
import seek.CompareList;

public class GeneEnrichmentCompareList extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		int top = Integer.parseInt(req.getParameter("top"));
		String goldstd = req.getParameter("goldstd");
		String toShowGenes = req.getParameter("show_overlap"); //used in combination of above

		String showOverlapOnly = req.getParameter("overlap_only"); //used with term parameter		
		String doTerm = req.getParameter("term");

		boolean showGenes;
		if(toShowGenes.equals("y")) showGenes = true;
		else showGenes = false;

		boolean overlapOnly;
		if(showOverlapOnly.equals("y")) overlapOnly = true;
		else overlapOnly = false;

		File tempDir = (File) getServletContext().
			getAttribute( "javax.servlet.context.tempdir" );

		String organism1 = req.getParameter("organism1");
		String organism2 = req.getParameter("organism2");

		String sessionID = req.getParameter("sessionID"); //tempDir

		String sessionID1 = req.getParameter("sessionID1");
		String sessionID2 = req.getParameter("sessionID2");

		String option = req.getParameter("option");
		String show_list = req.getParameter("show_list"); //shared, exclusive_to_left, or exclusive_to_right
		String show_all = req.getParameter("show_all");
		boolean showAll = false;
		if(show_all!=null && show_all.equals("true")){
			showAll = true;
		}

		boolean useSharedAsBackground = false;
		String background = req.getParameter("useSharedAsBackground");
		if(background!=null && background.equals("y")) useSharedAsBackground = true;

		

		CompareList cp = new CompareList();
		cp.Compare(organism1, organism2, sessionID1, sessionID2, tempDir.getAbsolutePath(), option, show_list, showAll);

		String organism = cp.final_organism;
		Vector<String> bg = null;
		if(useSharedAsBackground)
			bg = cp.final_background;
		GeneEnrichmentCore ge = null;

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");

		if(!overlapOnly){
			ge = new GeneEnrichmentCore(sessionID, top, cp.final_list, 
				organism, goldstd, bg, tempDir.getAbsolutePath(), false);
			for(int i=0; i<ge.returnString.size(); i++){
				res.getWriter().write(ge.returnString.get(i));
			}
		}else{
			//overlap only!
			ge = new GeneEnrichmentCore(sessionID, top, cp.final_list, 
				organism, goldstd, bg, tempDir.getAbsolutePath(), doTerm, false);
			res.getWriter().write(StringUtils.join(ge.returnString, " ") + "\n");
			System.out.println(ge.returnString);
		}

	}


}
