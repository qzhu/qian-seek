package seek;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageDownloadServlet extends HttpServlet {

  public void doGet( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException {
    // get the web applications temporary directory
    File tempDir = (File) getServletContext().
        getAttribute( "javax.servlet.context.tempdir" );

	String organism = req.getParameter("organism");
    // create a temporary file in that directory
    //File tempFile = File.createTempFile( getServletName(), ".tmp", tempDir );

	File tempFile = new File(tempDir + "/" + organism + "/" +  req.getParameter("file"));

	res.setContentType("image/png");
	FileInputStream in = new FileInputStream(tempFile);

	OutputStream os = res.getOutputStream();
	byte[] buf = new byte[1024];
	int count = 0;
	while((count=in.read(buf))>=0){
		os.write(buf, 0, count);
	}
	in.close();
	os.close();

  }

}
