
function JStorageSet(session_id, c_name, val){
	var t = 60*60*1000;
	$.jStorage.set(session_id + "_" + c_name, val, {TTL: t});
}

function JStorageGet(session_id, c_name){
	return $.jStorage.get(session_id + "_" + c_name, "null");
}

function JStorageDelete(session_id, c_name){
	$.jStorage.deleteKey(session_id + "_" + c_name);
}

function JStorageFlush(session_id){
	var ind = $.jStorage.index();
	for(var i=0; i<ind.length; i++){
		if(ind[i].indexOf(session_id)==0){
			$.jStorage.deleteKey(ind[i]);
		}
	}
}

function JStoragePrint(session_id){
	var ind = $.jStorage.index();
	alert("ind size: " + ind.length);
	alert($.jStorage.storageAvailable());
	alert($.jStorage.storageSize());
}
