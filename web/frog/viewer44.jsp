<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SEEK: Expression Viewer</title>

<link type="text/css" href="/css/viewer.css" rel="Stylesheet">
<link type="text/css" href="/css/jquery.qtip.css" rel="Stylesheet">
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/jquery.ui.touch-punch.min.js"></script>
<script src="/js/jquery.qtip.js"></script>
<script src="/js/jquery.ba-throttle-debounce.min.js"></script>

<script>
	var pageMode = "coexpression";
	var organism = "frog";
</script>

</head>

<body>
	<div id="wrapper" style="width:1100px;">
		<table id="wrapper_table">
			<tr><td style="padding:0px 0px 0px 0px;">
				<div id="query" style="background-color:rgb(0,128,128);">
					<div style="position:relative; top:1px; left:10px">
						<img src="/img/seek_frog.png" style="position:absolute" 
						height="58" width="119" />
					</div>

					<table>
						<tr>
							<td style="width:200px"></td>
							<td style="width:670px;color:white;">
								<span class="big_font">Query</span>
								<a href="#" id="main_query_tip" 
								class="help basic_font" onClick="return false;">
								<span id="span_main_query"
								style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;</span></a>:
								<input type="text" class="queryStyle" name="query_genes" 
								id="query_genes" size="50">
								<input id="next_button" type="button" value="Search">
								<a href="#" class="basic_font white" id="option_link">
								Options</a>
							</td>
							<td style="color:white;" class="basic_font" 
							id="status_message2"></td>
							<td style="width:200px;color:white;text-align:right;" 
								class="basic_font" id="home_panel">
								<!--About | 
								<a id="tutorial_enable" class="white" 
								href="viewer_tutorial.jsp">Tutorial</a>
								 | -->
								<a class="white" href="index.jsp">Home</a> 
							</td>
						</tr>
					</table>
				</div>
				<div id="screen_info" style="height:30px;background-color:rgb(0,128,128);">
					<table>
						<tr><td style="width:200px;"></td>
						<td style="width:300px;" class="basic_font">
							<div id="partial_message" style="border:1px solid black;text-align:center;width:350px;display:none;background-color:#e9ab17;color:white;">
									Co-expressed gene filter enabled.
							</div>
						</td>
						<td style="width:400px;"></td>
						<td id="expression_tab" class="seek-tab-unhighlighted basic_font" 
						style="height:25px;width:100px;">Expression
							<a href="#" id="main_expression_tip" 
							class="help basic_font" onClick="return false;">
							<span style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;</span></a>
						</td>
						<td style="width:5px;"></td>
						<td id="coexpression_tab" 
						class="seek-tab-highlighted basic_font" 
						style="height:25px;width:110px;">Co-expression
							<a href="#" id="main_coexpression_tip" 
							class="help basic_font" onClick="return false;">
							<span style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;</span></a>
						</td>
						<td style="width:5px;"></td>
					</table>
				</div>
				<table id="expression_message">
					<tr>
						<td style="width:900px;padding-left:20px;color:#707070;" 
						class="basic_font">
							SEEK has identified the following genes to be related to the 
							query by co-expression. Below is a heat map showing the degree of 
							co-expression in the top weighted datasets. 
							Tips: Hover, click on a gene label, or a dataset ID 
							label. Hover over the co-expression image. <br>
							<!--Use the Tutorial (top-right) for explanations.<br> -->
						</td>
						<td></td>
					</tr>
				</table>

				<table style="margin:0px;border-spacing:0px;">
					<tr style="height:30px;">
						<td style="width:200px;"></td>
						<td style="width:160px;" class="page-nav">
							<span class="gene_page_label basic_font">
							Genes </span>
						</td>
						<td style="width:5px;" class="page-nav"></td>
						<td style="width:20px;" class="page-nav">
							<a class="basic_font gene_next_page" href="#missing">
							<img src="/img/arrow_down.png" 
							style="vertical-align:middle;" class="arrow"></a>
						</td>
						<td style="width:5px;" class="page-nav"></td>
						<td style="width:20px;" class="page-nav">
						<a class="basic_font gene_prev_page" href="#missing">
							<img src="/img/arrow_up.png" 
							style="vertical-align:middle;" class="arrow"></a>
						</td>
						<td style="width:50px;" class="page-nav"></td>
						<td style="width:170px;" class="page-nav">
							<span class="dset_page_label basic_font">
							Datasets </span>
						</td>
						<td style="width:5px;" class="page-nav"></td>
						<td style="width:20px;" class="page-nav">
							<a class="basic_font dset_next_page" href="#missing">
							<img src="/img/arrow_right.png" 
							style="vertical-align:middle;" class="arrow"></a>
						</td>
						<td style="width:5px;" class="page-nav"></td>
						<td style="width:20px;" class="page-nav">
							<a class="basic_font dset_prev_page" href="#missing">
							<img src="/img/arrow_left.png" 
							style="vertical-align:middle;" class="arrow"></a>
						</td>
						<td style="width:100px"></td>
						<td></td>
						<td></td>
					</tr>
				</table>

				<table id="main_table" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td id="firstTd">
							<table id="col_header" cellspacing="0" cellpadding="0" 
							border="0">
								<tr>
								<td style="width:50px;height:170px;">
								</td>
								<td style="width:50px;">
								</td>
								<td id="main_weight" style="width:100px;vertical-align:top;">
									&nbsp;<br><u>Dataset rank,</u><br><u>keywords</u>
									<a href="#" id="main_weight_coexp_tip" class="help" onClick="return false;">
									<span id="span_main_dataset_head" 
									style="background-color:#e9ab17;font-weight:bold;">
									&nbsp;?&nbsp;
									</span>		
									</a>
								</td>
								</tr>
								<tr>
								<td colspan="3" style="text-align:right;">
									&nbsp;&nbsp;&nbsp;<b>Query Cross-Validations</b>
									<a href="#" id="main_cross_val_tip" class="help" onClick="return false;">
									<span style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;</span>		
									</a>
								</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<div id="divHeader" style="overflow:hidden; width:1100px; height:182px;">
								<div id="header_coord" style="cursor:pointer; position:relative;">
									<div style="z-index:0; position:absolute; top:0px; left:0px;">
										<img id="header_image_src" src="/img/loading.png">
									</div>
									<div id="rectangle_header"></div>
								</div>

							</div>
						</td>
					</tr>

					<tr>				
						<td valign="top">
							<div id="querycol">
								<table id="query_names" cellspacing="0" cellpadding="0" border="0">
								<!-- Fill IN -->
								</table>
							</div>
						</td>
						<td valign="top">
							<div id="query_table_div" style="overflow:hidden;">
								<table cellspacing="0" cellpadding="0" border="0">
									<tr id='firstTr'>
										<td><img style="z-index:1;" 
										id="query_microarray" src="/img/loading.png"></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>

				<tr>
				<td></td>
				<td>
				<table id="query_gradient_scale" style="margin-top:5px; border-spacing:0px;">
					<tr class="grad2">
						<td style="width:200px;"></td>
						<td id="gradient_3"
						style="height:10px; line-height:10px;" class="basic_font">&nbsp;</td>
						<!-- Fill in, Add <td>  -->
						<td id="gradient_4" class="small_font"
						style="width:100px;height:10px;color:#707070;">&nbsp;
							<a href="#" id="main_coexp_query_gradient_tip" class="help" onClick="return false;">
							<span style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;
							</span>		
							</a>
						</td>
						<!-- Fill in, Add <td>  -->
					</tr>
				</table>
				</td>
				</tr>

					<tr>
						<td class="small_font" style="text-align:right">
							&nbsp;&nbsp;&nbsp;<b>Gene-Query Co-expression</b>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<table id="col_header" cellspacing="0" cellpadding="0" 
							border="0">
								<tr>
								<td style="width:50px;">
									<u>Rank</u>
								</td>
								<td id="main_coexp" style="width:50px;">
									<u>Coexp</u>
									<a href="#" class="help" onClick="return false;">
									<span id="span_main_coexp" 
									style="background-color:#e9ab17;font-weight:bold;">
									&nbsp;?&nbsp;
									</span>		
									</a>
								</td>
								<td id="main_weight" style="width:100px;">
									<u>Gene</u>
								</td>
								</tr>
							</table>
						<td></td>
					</tr>
						
					<tr>			
						<td valign="top">
							<div id="firstcol" style="overflow:hidden;height:300px">
								<div id="gene_name_coord" style="cursor:pointer; position:relative;">
									<div style="z-index:0; position:absolute; top:0px; left:0px;">
										<table id="gene_names" 
										cellpadding="0" cellspacing="0" border="0">
										<!-- Fill IN -->
										</table>
									</div>
									<div id="rectangle_gene_header"></div>
								</div>
							</div>
						</td>
						<td valign="top">
							<div id="table_div" style="overflow:auto; width:1100px; height:300px">
								<div id="image_coord" style="cursor:pointer; position:relative;">
									<div style="z-index:0; position:absolute; top:0px; left:0px;">
										<img id="microarray" src="/img/loading.png">
									</div>
									<div id="rectangle"></div>
								</div>
							</div>
						</td>
					</tr>

				<tr>
				<td></td>
				<td>
				<table id="gradient_scale" style="margin-top:5px;border-spacing:0px;">
					<tr class="grad1">
						<td style="width:200px;"></td>
						<td id="gradient_1" 
						style="height:10px;line-height:10px;" class="basic_font">&nbsp;</td>
						<!-- Fill in, Add <td>  -->
						<td id="gradient_2" class="small_font" 
						style="width:100px;height:10px;color:#707070;">&nbsp;
							<a href="#" id="main_coexp_gene_gradient_tip" class="help" onClick="return false;">
							<span style="background-color:#e9ab17;font-weight:bold;">&nbsp;?&nbsp;
							</span>		
							</a>
						</td>
						<!-- Fill in, Add <td>  -->
					</tr>
				</table>
				</td>
				</tr>


				</table>

				<table style="margin-top:10px;border-spacing:0px;">
					<tr style="height:30px;">
						<td style="width:200px;"></td>
						<td style="vertical-align:middle; width:100px;" class="basic_font">
							<span><b>Analyses:</b></span>
						</td>
						<td style="vertical-align:middle; width:140px;" class="basic_font">
							<a href="#" id="enrich_genes">Enrichment of genes</a>
							<a href="#" class="help" onClick="return false;">
							<span id="span_enrich_gene" 
							style="background-color:#e9ab17;font-weight:bold;">
								&nbsp;?&nbsp;
							</span>		
							</a>
						</td>
						<td style="width:50px;"></td>
						<td style="vertical-align:middle;width:150px;" class="basic_font">
							<a href="#" id="select_dataset">Refine search</a>
							<a href="#" class="help" onClick="return false;">
							<span id="span_select_dataset" 
							style="background-color:#e9ab17;font-weight:bold;">
								&nbsp;?&nbsp;
							</span>		
							</a>
						</td>
						<td style="width:400px;"></td>
						<td style="display:none;vertical-align:middle;" class="basic_font">
							<a id="stretch_window" href="#">Resize...</a>
						</td>
					</tr>
				</table>

				<p>

				<table>
					<tr>
						<td style="width:20px;">&nbsp;</td>
						<td id="genes_complete" class="basic_font" style="color:#707070;">
							<a id="export_genes" href="#">See</a> 
							 the complete gene-list ranked by co-expression score
							<br>
							<a id="export_datasets" href="#">See</a> 
							 the complete dataset-list ranked by query-relevance
							<br>
							<a id="export_exp" href="#">Export</a> the co-expression matrices on this page
							<br>
						</td>
					</tr>
				</table>

				<div id="exportdiv" style="display:none;">
					<a id="export_download" href="#"></a>
				</div>
				<div id="somediv" style="display:none;"></div>
				<div id="overlapdiv" style="display:none;"></div>
				<div id="selectdiv" style="display:none;"></div>
				<br>
				<div name="dset_div" id="dset_div" style="display:none;"></div>
				<br>
				<div name="gene_div" id="gene_div" style="display:none;"></div>

				<div id="select_dataset_div" style="display:none;">
					Here are the list of datasets and their annotations.
				</div>
			</td>
		</tr>
	</table>
</div>

<div id="statusdiv" style="display:none;">
	<div id="status_message" style="display:none;">
		<font class="basic_font"><b>
		<span id="status_percentage"></span>
		<span id="status_text"></span>
		<img src="/ajax-loader.gif" style="vertical-align:text-bottom;">
		</b></font>
	</div>
	<div id="status_pane" style="height:500px; width:300px; overflow:auto;">
	<span id="dataset_result"></span>
	<br>
	<span id="search_result"></span>
	<br>
	</div>
	<a id="search_box" href="#"></a>
</div>

<div id="tutorial_tooltip" style="display:none;">
</div>

<div id="gene_tooltip" style="display:none:"></div>
<div id="dataset_tooltip" style="display:none:"></div>
<div id="image_tooltip" style="display:none:"></div>

<script src="/js/viewer_init.js"></script>
<script src="/js/ready_expression.js"></script>
<script src="/js/search.js"></script>
<script src="/js/viewer_common.js"></script>
<script src="/js/viewer_ready.js"></script>
<script src="/js/ready_expression_zoomin.js"></script>
<script src="/js/ready_enrichment.js"></script>
<script src="/js/ready_select_dataset.js"></script>
<script src="/js/ready_options.js"></script>

</body>
</html>

