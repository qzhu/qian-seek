package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import java.io.*;
import java.util.*;
import seek.GeneSet;
import seek.Pair;
import java.text.*;

public class GeneEnrichmentCore {

	public Vector<String> returnString;
	public Vector<String> returnGene;
	public Map<String, Integer> mentrez;
	public Map<Integer, String> mreverse;
	public SeekFile sf;
	public String goldstd; //goldstd type

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public void Prepare(String goldstd, String tempDir, String organism)throws IOException{
		this.goldstd = goldstd;
		this.sf = new SeekFile(tempDir, organism);
		this.sf.ReadGeneMap();
		Set<String> all_entrez = this.sf.ent_hgnc.keySet();
		this.mentrez = GeneSet.convertGeneNameToInteger(all_entrez);
		this.mreverse = new HashMap<Integer, String>();
		for(String s : mentrez.keySet())
			this.mreverse.put(mentrez.get(s), s);
	}

	public GeneEnrichmentCore(String sessionID, int top, Vector<String> genes, String organism,
	String goldstd, Vector<String> background, String tempDir, boolean isEntrez) throws IOException{
		returnString = new Vector<String>();
		returnGene = new Vector<String>();

		//prepare sf, mentrez, mreverse, goldstd
		Prepare(goldstd, tempDir, organism);
		Map<String,String> enrichment = this.sf.enrichment;

		//read gold standard gene set
		String tempDir1 = tempDir + "/" + organism;
		String s2 = tempDir1 + "/" + enrichment.get(this.goldstd);
		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2);

		if(isEntrez){
			Read(sessionID, top, genes, gold_std, background, tempDir);
		}
		else if(!isEntrez){
			Vector<String> g2 = new Vector<String>();
			Vector<String> b2 = null;
			for(String g : genes)
				g2.add(this.sf.hgnc_ent.get(g));
			if(background!=null && background.size()>0){
				b2 = new Vector<String>();
				for(String g : background)
					b2.add(this.sf.hgnc_ent.get(g));
			}
			Read(sessionID, top, g2, gold_std, b2, tempDir);
		}

	}

	//do overlap only!
	public GeneEnrichmentCore(String sessionID, int top, Vector<String> genes, String organism, 
	String goldstd, Vector<String> background, String tempDir, String doTerm,
	boolean isEntrez) throws IOException{
		returnString = new Vector<String>();
		returnGene = new Vector<String>();

		//prepare sf, mentrez, mreverse, goldstd
		Prepare(goldstd, tempDir, organism);
		Map<String,String> enrichment = this.sf.enrichment;

		//read gold standard gene set
		String s2 = tempDir + "/" + organism  + "/" + enrichment.get(this.goldstd);
		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2);

		if(!isEntrez){
			Vector<String> g2 = new Vector<String>();
			Vector<String> b2 = null;
			for(String g : genes)
				g2.add(this.sf.hgnc_ent.get(g));
			if(background!=null && background.size()>0){
				b2 = new Vector<String>();
				for(String g : background)
					b2.add(this.sf.hgnc_ent.get(g));
			}
			genes = g2;
			background = b2;
		}

		boolean defaultBackground = true;
		if(background!=null && background.size()>0)
			defaultBackground = false;

		if(!defaultBackground)
			gold_std = AdjustBackground(gold_std, background);

		Map<String, Vector<Integer> > gold_std_int = 
			GeneSet.convertGeneSetToInteger(gold_std, this.mentrez);

		Set<Integer> all_genes = new HashSet<Integer>();
		Set<String> all_gene_names = new HashSet<String>();

		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_genes.add(gs);
			for(String gg : gold_std.get(term))
				all_gene_names.add(gg);
		}
		int population = all_genes.size();

		int totAvailable = genes.size();	
		if(totAvailable<top)
			top = totAvailable;

		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		for(int i=0; i<top; i++){
			if(all_gene_names.contains(genes.get(i)) && 
			this.mentrez.containsKey(genes.get(i))){
				query.add(this.mentrez.get(genes.get(i)));
			}
		}
		Collections.sort(query);

		String term = new String(doTerm);
		Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
			query, gold_std_int.get(term));
		Set<String> si = new HashSet<String>();
		for(Integer ii : overlap)
			si.add(this.sf.ent_hgnc.get(this.mreverse.get(ii)));
		returnString = new Vector<String>(si);
		returnGene = new Vector<String>();
		Collections.sort(returnString);
	}

	public Map<String,Vector<String> > AdjustBackground(
	Map<String,Vector<String> > gold_std, Vector<String> background){
		Set<String> ss = new HashSet<String>(background);
		for(String term : gold_std.keySet()){
			Vector<String> gs = gold_std.get(term);
			Vector<String> new_gs = new Vector<String>();
			for(String ee : gs)
				if(ss.contains(ee))
					new_gs.add(ee);
			gold_std.put(term, new_gs);
		}
		return gold_std;
	}

	//assume that genes are already in ENTREZ
	//goldstd is the TERM type
	public void Read(String sessionID, int top, Vector<String> genes, 
	Map<String, Vector<String> > gold_std,
	Vector<String> background, String tempDir) throws IOException{	

		boolean defaultBackground = true;
		if(background!=null && background.size()>0)
			defaultBackground = false;

		if(!defaultBackground)
			gold_std = AdjustBackground(gold_std, background);

		Map<String, Vector<Integer> > gold_std_int = 
			GeneSet.convertGeneSetToInteger(gold_std, this.mentrez);

		Set<Integer> all_genes = new HashSet<Integer>();
		Set<String> all_gene_names = new HashSet<String>();

		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_genes.add(gs);
			for(String gg : gold_std.get(term))
				all_gene_names.add(gg);
		}
		int population = all_genes.size();

		int totAvailable = genes.size();	
		if(totAvailable<top){
			top = totAvailable;
		}

		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		for(int i=0; i<top; i++){
			if(all_gene_names.contains(genes.get(i)) && 
			this.mentrez.containsKey(genes.get(i))){
				query.add(this.mentrez.get(genes.get(i)));
			}
		}
		Collections.sort(query);

		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new 
			HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new 
				HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		String st = "/tmp/" + sessionID + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
			}
		}catch(Exception e){
			System.out.println("BAD IO 1");
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		String sv = "/tmp/" + sessionID + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		DecimalFormat myFormat = new DecimalFormat("0.##E0");
		double prev_value = 0;

		returnString = new Vector<String>();
		returnGene = new Vector<String>();

		//output results to client
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			double b_value = pi.val * gold_std_int.get(pi.term).size() / (double) (i+1);
			if(b_value>1)
				b_value = 1.0;
			if(b_value<prev_value)
				b_value = prev_value;
			prev_value = b_value;

			String decp = "";			
			String decq = "";
			if(this.goldstd.equals("targetscan") || this.goldstd.equals("targetscan_family")){
				if(map_overlap.get(pi.term).size()<=1) continue;
				if(pi.val>=0.05 || qval.get(i)>0.5) continue;
			}else{
				if(pi.val>=0.05 || qval.get(i)>0.25) break;
			}

			decp = myFormat.format(b_value);
			decq = myFormat.format(qval.get(i));
			returnString.add(pi.term + "\t" + decp + "\t" + decq +"\t" + 
				gold_std_int.get(pi.term).size() + "\t" + query.size() + "\t" + 
				map_overlap.get(pi.term).size() + "\n");

			//optional	
			for(Integer ii : map_overlap.get(pi.term))
				//for(String ss : this.sf.ent_hgnc.get(mreverse.get(ii)))
				//	returnGene.add(ss + " ");
				returnGene.add(this.sf.ent_hgnc.get(this.mreverse.get(ii)) + " ");
			returnGene.add("\n");
			
		}

	}
}
