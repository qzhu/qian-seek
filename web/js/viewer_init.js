function sessionSetDefault(arg1, arg2){
	if(sessionStorage.getItem(arg1)==null){
		sessionStorage.setItem(arg1, arg2);
	}
}

function init_commands(){
	document.getElementById("query_genes").value=queryHGNCNames.join(" ");
	fix_gradient();

	for(var vi=0; vi<geneNames.length; vi++){
		$("#gene_"+geneNames[vi]).find("a").qtip({
			style:{
				width:"300px", 
				classes:"ui-tooltip-genes", 
			}});
	}

	for(var vi=0; vi<queryNames.length; vi++){
		$("#query_"+queryNames[vi]).find("a").qtip({
			style:{
				width:"300px", 
				classes:"ui-tooltip-genes", 
			}});
	}

	for(var vi=0; vi<dsetNames.length; vi++){
		$("#dset_"+vi).find("a").qtip({
			style:{
				width:"300px", 
				classes:"ui-tooltip-genes", 
			}});
	}
	show_dataset_weight();
	activate_external_link();
	show_tutorial();
	//microarrayWindowWidth=950;
	//panelWindowWidth=1150;
	adjust_window();

$("#image_coord")
.unbind("mousemove").unbind("mouseleave")
.mousemove( $.throttle(100, false, function(e){
	var posX = $("#image_coord").offset().left;
	var X = e.pageX - posX;
	var x=0;
	var beginX=0;
	var endX=0;
	var dsetID=0;
	var geneID;
	for(var xi=0; xi<dsetNames.length; xi++){
		if(x+dsetSizes[xi]*horizUnit+horizSpacing>X){
			beginX = x;
			endX = beginX + dsetSizes[xi]*horizUnit+horizSpacing;
			dsetID = xi;
			break;
		}
		x+=dsetSizes[xi]*horizUnit + horizSpacing;		
	}

	var posY=$("#image_coord").offset().top;
	var Y = e.pageY - posY;
	var y = 0;
	var beginY=0;
	var endY=0;
	for(var yi=0; yi<geneNames.length; yi++){
		if(y+vertUnit>Y){
			beginY=y;
			geneID = yi;
			endY=beginY+vertUnit;
			break;
		}
		y+=vertUnit;
	}

	
	var borderColor = "";
	var color_scale = sessionStorage.getItem("vis_color_scale");
	if(color_scale=="green-black-red" || color_scale=="blue-black-yellow"){
		borderColor = "gray";
	}else{
		borderColor = "black";
	}

	//for mouse over of expression row
	$("#rectangle")
		.css("top", beginY+"px")
		.css("left", (beginX + 5) +"px")
		.css("width", (endX - beginX - 2 - horizSpacing) +"px")
		.css("height", (vertUnit-2)+"px")
		.css("border", "2px solid " + borderColor)
		.show();

	if(current_image_coord_X!=dsetID || current_image_coord_Y!=geneID){
		$("#image_tooltip").qtip("api").hide();
		setTimeout(function(){
			if(pageMode=="expression"){
				$("#image_tooltip").qtip("api").set("content.text", "Click for detail");
			}else{
				$("#image_tooltip").qtip("api").set("content.text", comat[dsetID][geneID]);
			}
			$("#image_tooltip").qtip("api").set("position.target", $("#rectangle"));
			$("#image_tooltip").trigger("click");	
		}, 100);
	}
	current_image_coord_X = dsetID;
	current_image_coord_Y = geneID;

}))
.mouseleave(function(){
	setTimeout(function(){
		$("#rectangle").hide();
		$("#image_tooltip").qtip("api").hide();
		current_image_coord_X = -1;
		current_image_coord_Y = -1;
	}, 100);
});


$("#image_tooltip")
.qtip("destroy")
.qtip({content:{text: "Hello",}, 
	position:{my: "top center", at: "bottom center",}, show:{event:"click",delay:1000,},
	style:{
		classes: "ui-tooltip-dark ui-tooltip-dark-label",
	},
	hide:false, events: {show: function(event, api){
		if(!$("#rectangle").is(":visible")){
			return false;
		}
	},},
});

$("#dataset_tooltip")
.qtip("destroy")
.qtip({content:{text: "Hello",}, 
	position:{my: "top left", at: "bottom left",}, show:{event:"click",delay:500,},
	style:{
		classes: "ui-tooltip-dark ui-tooltip-dark-label",
		//classes: "qtip-dark",
	},
	hide:false, events: {show: function(event, api){
		if(!$("#rectangle_header").is(":visible")){
			return false;
		}
	},},
});

$("#header_coord")
.unbind("mousemove").unbind("mouseleave")
.mousemove( $.throttle(100, false,
function(e){
	var posX = $("#header_coord").offset().left;
	var X = e.pageX - posX;
	var x=0;
	var beginX=0;
	var endX=0;
	var dsetID = 0;
	for(var xi=0; xi<dsetNames.length; xi++){
		//if coexpression, dsetSizes[xi] holds the pixel size
		//ie horizUnit = 1
		//horizSpacing = 0
		if(x+dsetSizes[xi]*horizUnit+horizSpacing>X){
			beginX = x;
			endX = beginX + dsetSizes[xi]*horizUnit+horizSpacing;
			dsetID = xi;
			break;
		}
		x+=dsetSizes[xi]*horizUnit + horizSpacing;		
	}

	var imgHeight = $("#header_coord").find("img").css("height");
	imgHeight = parseInt(imgHeight);

	var showBeginX = null;
	if(pageMode=="expression"){
		showBeginX = beginX;
	}else if(pageMode=="coexpression"){
		showBeginX = beginX + 4;
	} 

	//for mouse over of dataset header
	$("#rectangle_header")
		.css("top", "0px")
		.css("left", showBeginX+"px")
		.css("width", (endX - beginX - 2) +"px")
		.css("height", (imgHeight - 2) + "px")
		.show();

	if(current_dataset_tooltip==-1 ||
		current_dataset_tooltip!=dsetID){
		$("#dataset_tooltip").qtip("api").hide();
		setTimeout(function(){
			$("#dataset_tooltip").qtip("api").set("content.text", "<span class='small_font'>" + dsetDescriptionOneLineUnformatted[dsetID] + "</span><br><br>Title: " + dsetDescriptionTitleUnformatted[dsetID] + "<br>" + dsetPubmed[dsetID]);
			$("#dataset_tooltip").qtip("api").set("position.target", $("#rectangle_header"));
			$("#dataset_tooltip").trigger("click");	
		}, 100);
	}
	current_dataset_tooltip = dsetID;
	//alert("enter");
}
))
.mouseleave(function(){
	setTimeout(function(){
		$("#rectangle_header").hide();
		$("#dataset_tooltip").qtip("api").hide();
		current_dataset_tooltip = -1;
	}, 100);
	//alert("left");
});



//var geneDialogWidth = 500;
//var geneDialogHeight = 200;

$("#gene_tooltip").qtip("destroy")
.qtip({content:{text: "Hello",}, show:{event:"click",delay:500,},
	style:{
		classes: "ui-tooltip-dark ui-tooltip-dark-label",
	},
	hide:false, events: {show: function(event, api){
		if(!$("#rectangle_gene_header").is(":visible")){
			return false;
		}
	},},
});

$("#gene_name_coord")
.unbind("mousemove").unbind("mouseleave")
.mousemove( $.throttle(100, false,
function(e){
	var posY=$("#gene_name_coord").offset().top;
	var Y = e.pageY - posY;
	var y = 0;
	var beginY=0;
	var endY=0;
	var geneID = 0;
	for(var yi=0; yi<geneNames.length; yi++){
		if(y+vertUnit>Y){
			beginY=y;
			endY=beginY+vertUnit;
			geneID = yi;
			break;
		}
		y+=vertUnit;
	}

	//for mouse over of a gene header
	$("#rectangle_gene_header")
		.css("top", (beginY - 2)+"px")
		.css("left", "100px")
		.css("width", "96px")
		.css("height", "13px")
		.show();

	if(current_gene_tooltip==-1 ||
		current_gene_tooltip!=geneID){
		$("#gene_tooltip").qtip("api").hide();
		setTimeout(function(){
			$("#gene_tooltip").qtip("api").set("content.text", geneDescriptionOneLineUnformatted[geneID]);
			$("#gene_tooltip").qtip("api").set("position.target", $("#rectangle_gene_header"));
			$("#gene_tooltip").trigger("click");	
		}, 100);
	}
	current_gene_tooltip = geneID;
}))
.mouseleave(function(){
	setTimeout(function(){
		$("#rectangle_gene_header").hide();
		$("#gene_tooltip").qtip("api").hide();
		current_gene_tooltip = -1;
	}, 100);
});

//var dsetDialogWidth = 500;
//var dsetDialogHeight = 200;

}
