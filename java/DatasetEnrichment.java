package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;
import seek.GeneSet;
import seek.Pair;
import java.text.*;

public class DatasetEnrichment extends HttpServlet {

	public static void readDatasetPlatformMap(String file, 
		Map<String,Integer> dset_int, Map<Integer,String> int_dset)
		throws IOException {
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			while((s=in.readLine())!=null){
				//making dset_int, and int_dset
				StringTokenizer st = new StringTokenizer(s, "\t");
				String dataset = st.nextToken();
				dset_int.put(dataset, i);
				int_dset.put(i, dataset);
				i++;
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file x");
		}
	}

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			while ((s=in.readLine())!=null){
			}
			while ((s=err.readLine())!=null){
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			while ((s=in.readLine())!=null){
			}
			while ((s=err.readLine())!=null){
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}
	public static Vector<String> ReadRanks(String filename) throws IOException{
		Vector<String> ranks = new Vector<String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				st.nextToken();
				ranks.add(st.nextToken());
				st.nextToken();
			}
		}catch(IOException e){
			System.out.println("Error opening file " + filename);
		}
		return ranks;
	}

	//GSEXXX.GPLXXX (one line (space separated), or multi-line)
	public static Vector<Pair> ReadDatasetList(String filename) throws IOException{
		Vector<Pair> vp = new Vector<Pair>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			Vector<String> ss = new Vector<String>();
			while((s=in.readLine())!=null){
				ss.add(s);
			}
			StringTokenizer st = new StringTokenizer(ss.get(0), " ");
			boolean oneLine = false;
			if(st.countTokens()>=3){
				oneLine = true;
			}
			if(oneLine){
				i = 0;
				while(st.hasMoreTokens()){
					vp.add(new Pair(st.nextToken(), (float) i, -1));
					i++;
				}
			}else{
				i = 0;
				for(String xs : ss){
					Pair np = new Pair(xs, (float) i, -1);
					vp.add(np);
					i++;
				}
			}
		}catch(IOException e){
			System.out.println("Error opening file " + filename);
		}
		return vp;
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		String sessionID = req.getParameter("sessionID");
		int top = Integer.parseInt(req.getParameter("top"));
		String goldstd = req.getParameter("goldstd"); //goldstandard names
		String toShowDatasets = req.getParameter("show_overlap"); //used in combination of above

		String showOverlapOnly = req.getParameter("overlap_only"); //used with term parameter		
		String doTerm = req.getParameter("term");

		File tempDir = (File) getServletContext().getAttribute( "javax.servlet.context.tempdir" );
		String organism = req.getParameter("organism");

		boolean showDatasets = false;
		if(toShowDatasets.equals("y"))
			showDatasets = true;
		boolean overlapOnly = false;
		if(showOverlapOnly.equals("y"))
			overlapOnly = true;

		//boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
		//float pvalCutoff = Float.parseFloat(req.getParameter("pval_cutoff"));

		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
		Map<String,String> dataset_enrichment = sf.dataset_enrichment;
		String dset_plat_map_file = sf.dataset_platform_file;
		String annot_file = sf.getPath(dataset_enrichment.get(goldstd));
		File tempFile = new File(sf.getPath(sessionID + "_dweight"));

		//read dataset platform mapping
		String s1 = dset_plat_map_file;
		Map<String, Integer> dset_int = new HashMap<String,Integer>();
		Map<Integer, String> int_dset = new HashMap<Integer,String>();
		readDatasetPlatformMap(s1, dset_int, int_dset);
		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(annot_file); //although GeneSet, also works for dataset annotation reading
		Map<String, Vector<Integer> > gold_std_int = GeneSet.convertGeneSetToInteger(gold_std, dset_int);

		int population = 5000; //temporary
		Set<Integer> all_datasets = new HashSet<Integer>();
		Set<String> all_dataset_names = new HashSet<String>();

		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term)) all_datasets.add(gs);
			for(String gg : gold_std.get(term)) all_dataset_names.add(gg);
		}
		population = all_datasets.size();
		Vector<Pair> valid = null;
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		valid = ReadScore.SortScores(dset_int, sc, false); //false means false to negative correlations

		Vector<Pair> new_valid = new Vector<Pair>();
		for(int i=0; i<valid.size(); i++){
			if(valid.get(i).val==0f) continue;
			new_valid.add(valid.get(i));
		}
		valid = new_valid;

		//System.out.println(sc.length + " " + valid.size());
		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		if(top>valid.size()){
			System.out.println("Top exceed dataset list size, replace with max size.");
			top = valid.size();
		}
		for(int i=0; i<top; i++){
			if(all_dataset_names.contains(valid.get(i).term)){
				if(dset_int.containsKey(valid.get(i).term)) 
					query.add(dset_int.get(valid.get(i).term));
				//System.out.println(valid.get(i).term + "\t" + mentrez.get(valid.get(i).term));
			}
		}

		Collections.sort(query);

		if(overlapOnly){
			String term = new String(doTerm);
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			Set<String> si = new HashSet<String>();
			for(Integer ii : overlap){
				si.add(int_dset.get(ii));
			}
			Vector<String> vi = new Vector(si);
			Collections.sort(vi);
			String svi = StringUtils.join(vi, " ") + "\n";
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(svi);
			System.out.println(svi);
			return ;
		}
		
		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(query, gold_std_int.get(term));
			//System.out.println(term);
			//if(overlap.size()<=1) continue;
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		/*for(Pair p : vp){
			System.out.println(p.term + "\t" + p.val);
		}*/		
		String st = "/tmp/" + tempFile.getName() + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
				//System.out.println(pi.val);
			}
			//System.out.println("Done");
		}catch(Exception e){
			System.out.println("BAD IO 1");
			//e.printStackTrace();
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		String sv = "/tmp/" + tempFile.getName() + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		DecimalFormat myFormat = new DecimalFormat("0.##E0");

		boolean showNonSignificant = false;

		float prev_value = 0;
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			//float b_value = (float) pi.val * (float) gold_std_int.get(pi.term).size() / (float) (i+1);
			/*
			if(b_value>1f){
				b_value = 1.0f;
			}
			if(b_value<prev_value){
				b_value = prev_value;
			}
			prev_value = b_value;
			*/

			float b_value = (float) pi.val * (float) gold_std_int.keySet().size() / (float) (i+1);
			if(pi.val>=0.05) break;
			if(!showNonSignificant && qval.get(i)>0.25) continue;

			String decp = myFormat.format(pi.val); //p-value
			String decq = myFormat.format(qval.get(i));  //q-value
			res.getWriter().write(pi.term + "\t" + decp + "\t" + decq + "\t" + 
				gold_std_int.get(pi.term).size() + "\t" + query.size() + "\t" + 
				map_overlap.get(pi.term).size() + "\n");

			if(showDatasets){
				for(Integer ii : map_overlap.get(pi.term))
					res.getWriter().write(int_dset.get(ii) + " ");
				res.getWriter().write("\n");
			}
		}

	}
}
