package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.Network;
import seek.ReadPacket;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarrayImage {
    final static int maxCharHeight = 15;
    final static int minFontSize = 15;

    final static Color fg = Color.black;
    final static Color bg = Color.white;
    final static Color red = Color.red;
    final static Color white = Color.white;
    final static Color green = Color.green;
    final static Color black = Color.black;

    BufferedImage image;
	boolean useReference;
	boolean sortSamples;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = 
		new BasicStroke(1.0f, BasicStroke.CAP_BUTT, 
		BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    Dimension totalSize;
    FontMetrics fontMetrics;
    Random rnd;

    float[][][] matrix;
    float[][][] norm_matrix;
	float[][][] tmp_norm_matrix;    

    int[][][] color_red;
    int[][][] color_green;
    int[][][] color_blue;
    int[] size;
    
    int numDatasetToShow;
    int numGeneToShow;
    int windowWidth;
    int windowHeight;
    String[] names;
    String[] description;
    Vector<String> dsets;
    Vector<String> genes;
	Vector<Float> gene_score;
	Vector<Float> dataset_score;
    Map<String, String> map_genes;
    Map<String, String> map_datasets;
	Map<String, String> map_gene_symbol_entrez;

	String dset_order_file;   
 
    final static int horizontalDatasetSpacing = 2;
    //final static int horizontalDatasetSpacing = 10;
    
    final static int verticalGeneSpacing = 0;
    final static int geneNameWidth = 0;
    final static int verticalUnitHeight = 15;

    private int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }

	public int GetVerticalUnitHeight(){
		return verticalUnitHeight;
	}

	public int GetHorizontalUnitWidth(){
		return 2;
	}

	public int[] GetDatasetSize(){
		return size;
	}
    
	public int GetHorizontalDatasetSpacing(){
		return horizontalDatasetSpacing;
	}

	public int GetVerticalGeneSpacing(){
		return verticalGeneSpacing;
	}

    private int calculateUnitWidth(int s){
    	if(s<=10){
    		return 2;
    	}else if(s<=50){
    		return 2;
    	}else if(s<=200){
    		return 2;
    	}else if(s<=300){
    		return 2;
    	}else{
    		return 2;
    	}
    }

    public void calculateSize(){
    	int x = 5 + geneNameWidth;
    	int y = 0;
    	
    	for(int i=0; i<numDatasetToShow; i++){
    		x+=calculateWidth(size[i]) + horizontalDatasetSpacing;
    	}
    	x+=10;
    	
    	y += verticalUnitHeight*numGeneToShow;
    	//y += 30;
    	//y += numDatasetToShow * verticalUnitHeight;
    	
    	windowWidth = x;
    	windowHeight = y;
    }
    
    
    public int getWindowWidth(){
    	return windowWidth;
    }
    
    public int getWindowHeight(){
    	return windowHeight;
    }
    
    public void save(String name){
    	File f = new File(name);
    	try{
    		ImageIO.write(image, "png", f);
    	}catch(IOException e){	
    	}
    }
    
    public float[] getNormRow(float[] m){
    	int i;
    	float mean = 0;
    	float stdev = 0;
    	int num = 0;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		num++;
    		mean+=m[i];
    	}
    	if(num==0){
    		float[] r= new float[m.length];
    		for(i=0; i<m.length; i++){
    			r[i] = 327.67f;
    		}
    		return r;
    	}
    	mean/=(float)num;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		stdev+=(m[i] - mean)*(m[i] - mean);
    	}
    	stdev/=(float) num;
    	stdev = (float) Math.sqrt(stdev);
    	
    	float[] r= new float[m.length];
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f){
    			r[i] = 327.67f;
    			continue;
    		}
    		r[i] = (m[i] - mean) / stdev;
    		//System.out.printf("%.2f ", r[i]);
    	}
    	return r;
    }
    
    public void init(float[][][] mat) throws IOException{
        rnd = new Random();
        
        matrix = mat;
        int i, j, k;
       
        color_red = new int[matrix.length][][];
        color_green = new int[matrix.length][][];
        tmp_norm_matrix = new float[matrix.length][][];
        norm_matrix = new float[matrix.length][][];
        size = new int[matrix.length];

		//norm_matrix dimension: dataset, genes, samples        
        for(i=0; i<matrix.length; i++){ //dataset
        	tmp_norm_matrix[i] = new float[matrix[i].length][];
        	norm_matrix[i] = new float[matrix[i].length][];
        	color_red[i] = new int[matrix[i].length][];
        	color_green[i] = new int[matrix[i].length][];
        	size[i] = matrix[i].length;  //number of column
        	for(j=0; j<matrix[i].length; j++){ 
        		color_red[i][j] = new int[matrix[i][j].length];
        		color_green[i][j] = new int[matrix[i][j].length];
        		tmp_norm_matrix[i][j] = new float[matrix[i][j].length];
        		norm_matrix[i][j] = new float[matrix[i][j].length];
        	}

        	for(j=0; j<matrix[i][0].length; j++){ //number of genes
        		float[] mm = new float[size[i]];
        		for(k=0; k<size[i]; k++){
        			mm[k] = matrix[i][k][j];
        		}
        		//float[] xm = getNormRow(mm);
        		for(k=0; k<size[i]; k++){
					//if(sortSamples){
      		  			//tmp_norm_matrix[i][k][j] = xm[k];
      		  			tmp_norm_matrix[i][k][j] = mm[k];
					//}else{
						//norm_matrix[i][k][j] = xm[k];
					//	norm_matrix[i][k][j] = mm[k];
					//}
        		}
        	}
        }
        
		//Vector< Vector<Pair> > vp = new Vector< Vector<Pair> >();
		
		if(sortSamples && !useReference){
			PrintWriter out1 = new PrintWriter(new FileWriter(dset_order_file));
			
			for(i=0; i<matrix.length; i++){
				Vector<Pair> np = new Vector<Pair>();
				for(j=0; j<matrix[i].length; j++){
					float sum = 0;
					for(k=0; k<matrix[i][0].length; k++){
        				if(tmp_norm_matrix[i][j][k]>327.0f) continue;
						sum+=tmp_norm_matrix[i][j][k];
					}
					Pair p = new Pair(Integer.toString(j), (double) sum);
					np.add(p);
					//System.out.println(sum);
				}
				//System.out.println();
				Collections.sort(np);
				out1.print(dsets.get(i) + " ");
				for(j=0; j<np.size(); j++){
					if(j==np.size()-1)
						out1.print(np.get(j).term);
					else
						out1.print(np.get(j).term + " ");
				}
				out1.println();

				for(j=0; j<matrix[i].length; j++){
					int trans = Integer.parseInt(np.get(j).term);
					for(k=0; k<matrix[i][0].length; k++){
						norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][k];
					}
				}
			}

			out1.close();
		}
		else if(sortSamples && useReference){
			//sample_order is dset, sample_order dimension
			int[][] sample_order = new int[matrix.length][];
			for(i=0; i<matrix.length; i++){
				sample_order[i] = new int[matrix[i].length];
			}

			BufferedReader bb = new BufferedReader(new FileReader(dset_order_file));
			String s = null;
			int i_ind = 0;
			while((s=bb.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, " ");
				st.nextToken();
				int ki = 0;
				while(st.hasMoreTokens()){
					int order = Integer.parseInt(st.nextToken());
					sample_order[i_ind][ki] = order;
					ki++;
				}
				i_ind++;
			}
			bb.close();
			
			for(i=0; i<matrix.length; i++){
				for(j=0; j<matrix[i].length; j++){
					int trans = sample_order[i][j];
					for(k=0; k<matrix[i][0].length; k++){
						norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][k];
					}
				}
			}
		}
		else if(!sortSamples){
			PrintWriter out1 = new PrintWriter(new FileWriter(dset_order_file));
			for(i=0; i<matrix.length; i++){
				Vector<Pair> np = new Vector<Pair>();
				for(j=0; j<matrix[i].length; j++){
					Pair p = new Pair(Integer.toString(j), (double) j);
					np.add(p);
				}
				Collections.sort(np);
				out1.print(dsets.get(i) + " ");
				for(j=0; j<np.size(); j++){
					if(j==np.size()-1)
						out1.print(np.get(j).term);
					else
						out1.print(np.get(j).term + " ");
				}
				out1.println();
				for(j=0; j<matrix[i].length; j++){
					int trans = Integer.parseInt(np.get(j).term);
					for(k=0; k<matrix[i][0].length; k++){
						norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][k];
					}
				}
			}
			out1.close();
		}

        names = new String[genes.size()];
        float clip = 3.0f;
        for(i=0; i<matrix.length; i++){
        	for(j=0; j<matrix[i].length; j++){
        		for(k=0; k<matrix[i][j].length; k++){        	
        			//matrix[i][j][k] = rnd.nextFloat()*10.0f;
        			color_red[i][j][k] = 0;
        			color_green[i][j][k] = 0;
        			if(norm_matrix[i][j][k]>327.0f){
        				color_green[i][j][k] = 128;
        				color_red[i][j][k]= 128;
        				continue;
        			}
        			if(norm_matrix[i][j][k]>0.0f){
        				if(norm_matrix[i][j][k]>clip){
        					color_red[i][j][k] = 255;
        				}else{
        					color_red[i][j][k] = (int) (1.0f * 
								norm_matrix[i][j][k] / clip * 255.0f);
        				}
        			}else if(norm_matrix[i][j][k]<0.0f){
        				if(norm_matrix[i][j][k]<-1.0f*clip){
        					color_green[i][j][k] = 255;
        				}else{
        					color_green[i][j][k] = (int) (-1.0f * 
								norm_matrix[i][j][k] / clip * 255.0f);
        				}
        			}
        		}
        	}
        }
       
		//try{ 
        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }
		/*}catch(Exception e){
			System.out.println("Exception: " + genes.get(i));
			if(!map_genes.containsKey(genes.get(i))){
				System.out.println("map_genes does not contain key!");
			}
		}*/
        
        int STRING_MAX_LENGTH = 200;
        description = new String[numDatasetToShow];
        
        for(k=0; k<numDatasetToShow; k++){
        	String stem = getStem(dsets.get(k));
        	String descr = map_datasets.get(stem);
        	if(descr.length()>STRING_MAX_LENGTH){
        		descr = descr.substring(0, 200) + "...";
        	}
        	description[k] = "#" + (k+1) + "  " + stem + ": " + descr;
        }
        
        /*for(i=0; i<names.length; i++){
        	char[] ca = new char[10];
        	for(j=0; j<ca.length; j++){
        		if(j%2==0){
        			int ri = rnd.nextInt(2);
        			int cc = rnd.nextInt(26);
        			if(ri==0){
        				ca[j] = 'a';
        				ca[j] += cc;
        			}else{
        				ca[j] = 'A';
        				ca[j] += cc;
        			}
        		}else{
        			int ri = rnd.nextInt(10);
        			ca[j] = '0';
        			ca[j] += ri;
        		}
        	}
        	names[i] = new String(ca);
        }*/
        
        //new
        calculateSize();
        image = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
    }

    FontMetrics pickFont(Graphics2D g2,
                         String longString,
                         int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();

        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            }
            else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

    public void paint() {
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);
       // Dimension d = getSize();
        int gridWidth = (int) (windowWidth / 30.0);
        int gridHeight = (int) (windowHeight / 20.0);

        fontMetrics = pickFont(g2, "Filled and Stroked GeneralPath",
                               gridWidth);

        Color fg3D = Color.lightGray;

        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, windowWidth, windowHeight));
        g2.setPaint(fg);

        int x=5;
        int y=15;
        int rectWidth = gridWidth;
        int rectHeight = gridHeight;
        
        int i, j, k;
        //x = 5 + geneNameWidth;
        
        /*int maxDatasetWidth = 0;
        for(k=0; k<numDatasetToShow; k++){
        	int strwidth = g2.getFontMetrics().stringWidth(description[k]);
        	if( strwidth > maxDatasetWidth)
        		maxDatasetWidth = strwidth;
        }
        maxDatasetWidth+=50;
        
        int numCol = (windowWidth - geneNameWidth) / maxDatasetWidth;
        if(numCol==0) numCol = 1;
        int rowsPerCol = numDatasetToShow / numCol;
        if(rowsPerCol%numCol>0){
        	rowsPerCol++;
        }*/

        /*int kk = 0;
        g2.setPaint(fg);
        int initialY = y;
        for(int nc = 0; nc<numCol; nc++){
        	y = initialY;
        	for(k=0; k<rowsPerCol && kk<numDatasetToShow; k++){
        		x = 5 + geneNameWidth + nc * maxDatasetWidth;
        		g2.drawString(description[kk], x, y);
        		y+=verticalUnitHeight;
        		kk++;
        	}
        }
        
        y=initialY + rowsPerCol * verticalUnitHeight + 10;*/
        int[] unitWidth = new int[size.length];
        for(i=0; i<size.length; i++){ //dataset
        	unitWidth[i] = calculateUnitWidth(size[i]);
        }
        /*
        x = 5 + geneNameWidth;
        for(i=0; i<numDatasetToShow; i++){
        	g2.drawString("#" + (i+1), x, y);
        	x += unitWidth[i] * size[i] + horizontalDatasetSpacing;
        }
        
        y+=5;*/
		y = 0;
        	
        for(k=0; k<numGeneToShow; k++){
        	x = 5;
        	g2.setPaint(fg);
        	g2.drawString(names[k], x, y+10);
			System.out.println(names[k]);
        	x += geneNameWidth;
        	for(i=0; i<size.length; i++){ //dataset
        		for(j=0; j<size[i]; j++){ //column in dataset
        			if(matrix[i][j][k]>327f){
        				g2.setPaint(new Color(128, 128, 128));
        			}else{
        				g2.setPaint(new Color(
        					color_red[i][j][k], color_green[i][j][k], 0));
        			}
        			g2.fill(new Rectangle2D.Double(x, y, unitWidth[i], 
        				verticalUnitHeight));
        			x+=unitWidth[i];
        		}
        		x+=horizontalDatasetSpacing;
        	}
        	y+=verticalUnitHeight;
        }
        
        
    }
   
	public String[] getGeneNames(){
		return names;
	}
 
    public String getStem(String s){
    	int i = s.indexOf("_");
    	int j = s.indexOf(".");
    	if(i!=-1){
    		return s.substring(0, i);
    	}
    	return s.substring(0, j);
    }
    
    public Vector<String> getDatasets(){
    	return dsets;
    }
    
    public Vector<String> getGenes(){
    	return genes;
    }
   
	public Vector<Float> getGeneScore(){
		return gene_score;
	}

	public Vector<Float> getDatasetScore(){
		return dataset_score;
	}
 
    public byte[] getDatasetByte() throws IOException{
		String strDatasets = StringUtils.join(dsets, "\t");
		return Network.getStringByte(strDatasets);
    	/*int size = 4;
    	int i, j;
    	for(i=0; i<dsets.size(); i++){
    		size += dsets.get(i).length();
    	}
    	size += dsets.size(); //for tabs and final '\0' character
    	byte[] b = new byte[size];
    	//System.out.println(Integer.toHexString(size));
    	
    	byte[] ii = intToByteArray(size-4);
    	int k = 0;
    	for(i=0; i<4; i++){
    		b[k] = ii[i];
    		//System.out.printf("%2X ", b[k]);
    		k++;
    	}
    	
    	for(i=0; i<dsets.size(); i++){
    		for(j=0; j<dsets.get(i).length(); j++){
    			b[k] = (byte) dsets.get(i).charAt(j);
    			//System.out.printf("%2X ", b[k]);
    			k++;
    		}
    		if(i==dsets.size()-1){
    			b[k] = (byte) '\0';
    			//System.out.printf("%2X ", b[k]);
    		}else{
    			b[k] = (byte) '\t';
    			//System.out.printf("%2X ", b[k]);
    		}
    		k++;
    	}
    	System.out.println(size + " " + k);*/
    	//return b;
    }
    
    public byte[] intToByteArray(int value) {
    	ByteBuffer buf = ByteBuffer.allocate(10);
    	buf.order(ByteOrder.LITTLE_ENDIAN);
    	buf.putInt(value);
    	return new byte[] {
    		buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
    }
    
    public byte[] getGeneByte() throws IOException{
		String strGenes = StringUtils.join(genes, "\t");
		return Network.getStringByte(strGenes);
    	/*int size = 4;
    	int i, j;
    	for(i=0; i<genes.size(); i++){
    		size += genes.get(i).length();
    	}
    	size += genes.size(); //for tabs and final '\0' character
    	byte[] b = new byte[size];
    	
    	byte[] ii = intToByteArray(size-4);
    	int k = 0;
    	for(i=0; i<4; i++){
    		b[k] = ii[i];
    		k++;
    	}
    	
    	for(i=0; i<genes.size(); i++){
    		for(j=0; j<genes.get(i).length(); j++){
    			b[k] = (byte) genes.get(i).charAt(j);
    			k++;
    		}
    		if(i==genes.size()-1){
    			b[k] = (byte) '\0';
    		}else{
    			b[k] = (byte) '\t';
    		}
    		k++;
    	}
    	return b;*/
    }

	public boolean ReadMap(String gm, String dm) throws IOException{
    	Scanner sc = null;

    	map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    		//System.out.println(s2 + " " + s3);
    	}finally{
    		sc.close();
    	}

    	map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}

		return true;

	}


	public boolean readQuery(String tempDir,
		String query_path, String dataset_path,	
		int numD, int numG, int dPage, int gPage) throws IOException{

		Map<String, Integer> mm = null;
		int gStart, dStart;
		gStart = (gPage - 1) * numG;
		dStart = (dPage - 1) * numD;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		Scanner ss = null;

		try{
			map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(tempDir + "/gene_entrez_symbol.txt");
    	
			name_mapping = tempDir + "/dataset_platform.jul7";
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc);
			
			for(int i=0; i<numD; i++){
				dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dataset_score.add(new Float(vp.get(dStart + i).val));
				System.out.println("Dataset " + vp.get(dStart + i).term);
			}

    		ss = new Scanner(new BufferedReader(new FileReader(query_path)));
    		String s1 = ss.nextLine();
    		StringTokenizer st = new StringTokenizer(s1);
			while(st.hasMoreTokens()){
				genes.add(st.nextToken());
			}
			ss.close();

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

	//	System.out.println("Query: " + genes);
    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = genes.size();
    	
		return true;    
	}

	//for query image, set sortSamples = true, and useReference = false 
	//(which will generate a reference)
 	//for coexpressed image, set sortSamples = true, and useReference = true
	//(will use an existing reference)
	public boolean read(String tempDir,
		String gene_path, String dataset_path,
		//String query, 
		int numD, int numG, 
		int dPage, int gPage) throws IOException{
    		
		Map<String, Integer> mm = null;
		int gStart, dStart;
		gStart = (gPage - 1) * numG;
		dStart = (dPage - 1) * numD;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();

		try{
			map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(tempDir + "/gene_entrez_symbol.txt");
    	
			name_mapping = tempDir + "/dataset_platform.jul7";
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc);
			
			for(int i=0; i<numD; i++){
				dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dataset_score.add(new Float(vp.get(dStart + i).val));
				System.out.println("Dataset " + vp.get(dStart + i).term);
			}
			
    		name_mapping = tempDir + "/gene_id.map";
    		name_mapping_2 = tempDir + "/gene_entrez_symbol.txt";
    		mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
			sc = ReadScore.ReadScoreBinary(gene_path);
			vp = ReadScore.SortScores(mm, sc);

			for(int i=0; i<numG; i++){
				genes.add(map_gene_symbol_entrez.get(vp.get(gStart + i).term));
				gene_score.add(new Float(vp.get(gStart + i).val));
				System.out.println("Gene " + map_gene_symbol_entrez.get(vp.get(gStart + i).term));
			}


		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

	//	System.out.println("Query: " + genes);
    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = genes.size();
    	
		return true;    
	
    }
    
    public boolean Load(String queryFile, String imageFile, 
		int numDPerPage, int numGPerPage, int dPageNum, int gPageNum, 
		boolean sortSamples, boolean useReference, boolean binaryInput) 
		throws IOException{

    	Socket kkSocket = null;
		this.sortSamples = sortSamples;
		this.useReference = useReference;    	

		File ff = new File(queryFile);
		String session = ff.getName();
		int session_index = session.indexOf("_");
		String sessionID = null;

		if(session_index==-1){
			sessionID = new String(session);
		}else{
			sessionID = session.substring(0, session_index);
		}

		//if((sortSamples && !useReference) || (sortSamples && useReference)){
		dset_order_file = ff.getParent() + "/" + sessionID + "_dset_order";

		String tempDir = ff.getParent();

		if(binaryInput){
 	       if(!this.read(tempDir, ff.getParent() + "/" + sessionID + "_gscore", 
				ff.getParent() + "/" + sessionID + "_dweight", 
				numDPerPage, numGPerPage, dPageNum, gPageNum)){
				return false;
			}
		}else{
			if(!this.readQuery(tempDir, ff.getParent() + "/" + sessionID + "_query",
				ff.getParent() + "/" + sessionID + "_dweight", 
				numDPerPage, numGPerPage, dPageNum, gPageNum)){
				return false;
			}
		}

        byte[] dByte = this.getDatasetByte();
        byte[] gByte = this.getGeneByte();
		byte[] modeByte = Network.getStringByte("01");

        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        int[] datasetSize;
        byte[] ss = null;
        float[][][] mat = null;
        ByteBuffer buf = null;
		ReadPacket st = null;
    	int num_datasets = dsets.size();
    	int num_genes = genes.size();
       
    	try{
    		kkSocket = new Socket("localhost", 9001);
    		kkSocket.getOutputStream().write(modeByte, 0, modeByte.length);
    		kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    		kkSocket.getOutputStream().write(dByte, 0, dByte.length);
    		
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			datasetSize = new int[num_datasets];
			for(int i = 0; i<num_datasets; i++){
				datasetSize[i] = st.vecFloat.get(i).intValue();
			}

			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			Vector<Float> geneExpr = st.vecFloat;

    		mat = new float[num_datasets][][];
			int kk=0;
    		for(int i=0; i<num_datasets; i++){
    			mat[i] = new float[datasetSize[i]][];
    			for(int k=0; k<datasetSize[i]; k++){
    				mat[i][k] = new float[num_genes];
    			}
    			
    			for(int g=0; g<num_genes; g++){
    				for(int k=0; k<datasetSize[i]; k++){
    					mat[i][k][g] = geneExpr.get(kk);
						if(mat[i][k][g]==-9999){
							mat[i][k][g] = 327.67f;
						}
						kk++;
    					//System.out.printf("%.2f\n", mat[i][k][g]);
    				}
    			}
    		}
    		
    		this.init(mat);
    		this.paint();
    		this.save(imageFile);
    		
    	}catch(IOException e){
    	}

		return true;
    	 //applet.init();
        //applet.paint();
        //applet.save("test.png");
    }

}
