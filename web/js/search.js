	//sessionID is a global variable
	function poll_progress(){
		$.get("../servlet/GetSearchMessage", {organism:organism, param:"status", sessionID:sessionID}, function(responseText){
			//alert(responseText);
			var a = responseText.split("\n");
			a.pop();
			//alert(a[a.length-1]);
			$("#search_result").html(a.join("<br>"));
			$("#status_pane").scrollTop($("#status_pane")[0].scrollHeight);
			if(a[a.length-1].indexOf("Error: no dataset contains")==0){
				alert("Error: no dataset contains any of the query genes. Please go back and fix your query.");
				document.location.reload();
			}else if(a[a.length-1].indexOf("Error: not enough query genes present")==0){
				alert("Error: not enough query genes present in >70% of the datasets. Suggest restricting datasets to those containing all query genes, or change query.");
				document.location.reload();
			}else if(a[a.length-1]=="Done writing results."){
				$("#status_percentage").text("100%");
				$("#status_text").text("Finished");
				$("#status_pane").scrollTop($("#status_pane")[0].scrollHeight);
				resetGenes();
			}else{
				setTimeout(poll_progress, 1000);
			}
		});
	}

	$("#search_box").click(function(){
		//alert("Doing search");
		var queryUpper = $("#query_genes").attr("value").toUpperCase();
		queryUpper = queryUpper.replace(/;/g, " ").replace(/,/g, " ")
						.replace(/"/g, " ").replace(/\s{2,}/g, " ");
		var d = new Date();
		var n = d.getTime();
		sessionID = n.toString();
	
		$.get("../servlet/GetSearchMessage", {param:"status", sessionID:sessionID}, function(resT){	
		$.post("../servlet/DoSearch2", {organism:organism, sessionID:sessionID, dset:"all",
			query:queryUpper, query_mode:"gene_symbol", 
			search_alg:sessionStorage.getItem("search_alg"), 
			search_distance:sessionStorage.getItem("search_distance"),
			rbp_p:sessionStorage.getItem("rbp_p"), 
			correlation_sign:"positive",
			percent_query:sessionStorage.getItem("percent_query"),
			percent_genome:sessionStorage.getItem("percent_genome")}, 
			function(responseText){}
		);
		});

		setTimeout(poll_progress, 1000);
		return false;
	});
	
	function resetGenes(){
		$.get("../servlet/GetSearchMessage", {organism:organism, param:"query_attr", sessionID:sessionID}, function(responseText){
			var res = responseText.split("\n");
			if(res[res.length-1]==""){
				res.pop();	
			}
			queryNames = [];
			for(var vi=0; vi<res.length; vi++){
				queryNames.push(res[vi]);
			}
			//sessionStorage.clear();
			sessionStorage.setItem("gene_refine_list", "");
			sessionStorage.setItem("dataset_refine_list", "");
			sessionStorage.setItem("specify_genes", "");
			sessionStorage.setItem("gene_refine_list", queryNames.join(";")); //query by default is still in query
			$("#vis_result").show();
			$("#statusdiv").qtip("hide");
			window.location.href = "viewer33.jsp?sessionID=" + sessionID + 
			"&sort_sample_by_expr=true";			
		//});
		});
	}
	
	$("#statusdiv").qtip({
		id: "statusdiv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading...",
				button: false,
			},
		},
		position: {
			my: "center", 
			at: "center", 
			target: $(window),
		},
		show:{
			event: "click", 
			solo: true,
			modal: {
				on: true,
				blur: false,
			},
			effect: false,
		},
		hide: false,
		style: {
			//classes: "ui-tooltip-dark",
			classes: "ui-tooltip-light",
		},
	});

	function category_search2(dset_selection, isCategory){
		var nn = [];
		if(isCategory) 
			nn.push("category");
			
		for(var i=0; i<dset_selection.length; i++)
			nn.push(dset_selection[i]);
		
		$("#dataset_result").html("");
		$("#search_result").html("");
		$("#vis_result").hide();
		$("#status_percentage").text("33%");				
		$("#status_text").text("Looking for datasets...");
		//sessionStorage.clear();
		$("#status_text").text("Performing search...");
		$("#statusdiv").qtip("option", {"content.text": $("#statusdiv")});
		$("#statusdiv").qtip("api").set("content.title.text", $("#status_message"));
		$("#statusdiv").trigger("click");
		
		$("#status_percentage").text("66%");				
		var queryUpper = $("#query_genes").attr("value").toUpperCase();
		var d = new Date();
		var n = d.getTime();
		sessionID = n.toString();
		
		$.get("../servlet/GetSearchMessage", {param:"status", sessionID:sessionID}, function(resT){	
		$.post("../servlet/DoSearch2", {organism:organism, sessionID:sessionID, dset:nn.join(";"),
			query:queryUpper, query_mode:"gene_symbol", 
			search_alg:sessionStorage.getItem("search_alg"), 
			search_distance:sessionStorage.getItem("search_distance"),
			rbp_p:sessionStorage.getItem("rbp_p"),
			correlation_sign:"positive",
			percent_query:sessionStorage.getItem("percent_query"), 
			percent_genome:sessionStorage.getItem("percent_genome")}, 
			function(responseText){}
		);
		});

		setTimeout(poll_progress, 1000);
		return false;
	}

	$("#next_button").click(function(){
		//alert($("#query_genes").attr("value"));
		if(sessionStorage.getItem("remember_dset_selection")!=null &&
			sessionStorage.getItem("remember_dset_selection")=="true"){	
	
			if(sessionStorage.getItem("dset_search_mode")!=null){
				var mode = sessionStorage.getItem("dset_search_mode");
				if(mode=="category"){
					var s = sessionStorage.getItem("datasetname_select_list");
					var ss = s.split(";");
					if(ss!=""){
						category_search2(ss, true);
						return false;
					}
				}else if(mode=="rank"){
					var s = sessionStorage.getItem("export_dset_select_list");
					var ss = s.split(";");
					if(ss!=""){
						category_search2(ss, false);
						return false;
					}
				}
			}
		}

		sessionStorage.setItem("dset_search_mode", "all");
		$("#dataset_result").html("");
		$("#search_result").html("");
		$("#vis_result").hide();
		$("#status_percentage").text("33%");				
		$("#status_text").text("Looking for datasets...");
		//sessionStorage.clear();
		$("#status_text").text("Performing search...");
		$("#statusdiv").qtip("option", {"content.text": $("#statusdiv")});
		$("#statusdiv").qtip("api").set("content.title.text", $("#status_message"));
		$("#statusdiv").trigger("click");

		$("#status_percentage").text("66%");				
		$("#search_box").trigger("click");
		
		return false;	
	});
