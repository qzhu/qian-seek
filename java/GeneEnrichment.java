package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import seek.GeneSet;
import seek.Pair;
import java.text.*;

public class GeneEnrichment extends HttpServlet {

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
			//p.waitFor();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		String sessionID = req.getParameter("sessionID");
		int top = Integer.parseInt(req.getParameter("top"));
		String goldstd = req.getParameter("goldstd");
		String toShowGenes = req.getParameter("show_overlap"); //used in combination of above

		String showOverlapOnly = req.getParameter("overlap_only"); //used with term parameter		
		String doTerm = req.getParameter("term");
		boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
		float pvalCutoff = Float.parseFloat(req.getParameter("pval_cutoff"));

		Vector<String> onlyGenes = null;
		String specifyGenes = null;
		if(req.getParameter("specify_genes")!=null){
			specifyGenes = java.net.URLDecoder.decode(
				req.getParameter("specify_genes"), "UTF-8");
			String[] xs = StringUtils.split(specifyGenes);
			if(xs.length>0){
				onlyGenes = new Vector<String>();
				for(String x : xs)
					onlyGenes.add(x);
			}
		}

		boolean showGenes;
		if(toShowGenes.equals("y"))
			showGenes = true;
		else
			showGenes = false;

		boolean overlapOnly;
		if(showOverlapOnly.equals("y"))
			overlapOnly = true;
		else
			overlapOnly = false;

		File tempDir = (File) getServletContext().
			getAttribute( "javax.servlet.context.tempdir" );
		String organism = req.getParameter("organism");

		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
		String s1 = sf.gene_entrez_map_file;
		Map<String, Vector<String> > mge = GeneSet.readGeneEntrezMapping(s1);
		Map<String, Vector<String> > meg = GeneSet.convertGeneEntrez(mge);
		Set<String> all_entrez = meg.keySet();
		Map<String, Integer> mentrez = 
			GeneSet.convertGeneNameToInteger(all_entrez);
		Map<Integer, String> mreverse = new HashMap<Integer, String>();
		for(String s : mentrez.keySet())
			mreverse.put(mentrez.get(s), s);

		Map<String,String> enrichment = sf.enrichment;

		//read gold standard gene set
		String s2 = sf.getPath(enrichment.get(goldstd));
		/*
		if(goldstd.equals("msigdb_curated")){
			s2 = tempDir1 + "/" + "msigdb.curated.gene.set.oct11";
		}else if(goldstd.equals("msigdb_comput")){
			s2 = tempDir1 + "/" + "msigdb.computational.gene.set.oct11";
		}else if(goldstd.equals("msigdb_chr")){
			s2 = tempDir1 + "/" + "msigdb.positional.gene.set.oct11";
		}else if(goldstd.equals("netcompare")){
			s2 = tempDir1 + "/" + 
				"gene_ontology_combined_experimental_netcompare.sept28";
		}else if(goldstd.equals("original")){
			s2 = tempDir1 + "/" + "all_gene_ontology_annot.apr1.entrez";
		}else if(goldstd.equals("withIEA")){
			s2 = tempDir1 + "/" + "all_IEA_annot.entrez";
		}else if(goldstd.equals("msigdb_miRNA")){
			s2 = tempDir1 + "/" + "msigdb.separate.miRNA.gene.set.oct11";
		}else if(goldstd.equals("targetscan")){
			s2 = tempDir1 + "/" + "targetscan.conserved.entrez";
		}else if(goldstd.equals("targetscan_family")){
			s2 = tempDir1 + "/" + "targetscan.family.conserved.entrez";
		}else if(goldstd.equals("msigdb_go_bp")){
			s2 = tempDir1 + "/" + "msigdb.separate.bp.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_cc")){
			s2 = tempDir1 + "/" + "msigdb.separate.cc.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_mf")){
			s2 = tempDir1 + "/" + "msigdb.separate.mf.gene.set.oct11";
		}else if(goldstd.equals("msigdb_biocarta")){
			s2 = tempDir1 + "/" + "msigdb.separate.biocarta.gene.set.oct11";
		}else if(goldstd.equals("msigdb_kegg")){
			s2 = tempDir1 + "/" + "msigdb.separate.kegg.gene.set.oct11";
		}else if(goldstd.equals("msigdb_reactome")){
			s2 = tempDir1 + "/" + "msigdb.separate.reactome.gene.set.oct11";
		}else if(goldstd.equals("msigdb_cgp")){
			s2 = tempDir1 + "/" + "msigdb.separate.cgp.gene.set.oct11";
		}
		*/
		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2);
		Map<String, Vector<Integer> > gold_std_int = 
			GeneSet.convertGeneSetToInteger(gold_std, mentrez);

		int population = 18000; //temporary
		Set<Integer> all_genes = new HashSet<Integer>();
		Set<String> all_gene_names = new HashSet<String>();
		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_genes.add(gs);
			for(String gg : gold_std.get(term))
				all_gene_names.add(gg);
		}
		population = all_genes.size();

		//reading gene names sorted by gene scores
		File tempFile = new File(sf.getPath(sessionID + "_gscore"));
		String name_mapping = sf.gene_map_file;
		boolean negative_cor = ReadScore.isNegativeCor(sf.leading_path, sessionID);

		Map<String, Integer> mm = ReadScore.readGeneMapping(name_mapping);		
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		String pval_path = sf.getPath(sessionID + "_pval");
		float[] sc_pval = null;
		File test_file = new File(pval_path);
		if(test_file.exists()){
			sc_pval = ReadScore.ReadScoreBinary(pval_path);
		}
		//filter by pval
		if(filter_by_pval && sc_pval!=null){
			for(int i=0; i<sc.length; i++){
				if(sc_pval[i]>pvalCutoff){
					sc[i] = -320.0f;
				}
			}
		}
		//filter by user-specified genes
		if(onlyGenes!=null && onlyGenes.size()>0){
			//hgnc to ID mapping
			Map<String, Integer> mt = ReadScore.readGeneMapping(
				sf.gene_map_file, sf.gene_entrez_map_file);	
			Set<Integer> include_id = new HashSet<Integer>();
			for(int i=0; i<onlyGenes.size(); i++){
				int internal_id = mt.get(onlyGenes.get(i));
				include_id.add(internal_id);
			}
			for(int i=0; i<sc.length; i++){
				if(!include_id.contains(i)){
					sc[i] = -320.0f;
				}
			}
		}

		Vector<Pair> valid = ReadScore.SortScores(mm, sc, negative_cor);

		int totAvailable = 0;	
		for(int i=0; i<valid.size(); i++){
			if(valid.get(i).val==0f) break;
			totAvailable++;
		}
		if(totAvailable<top){
			top = totAvailable;
		}

		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		for(int i=0; i<top; i++){
			if(all_gene_names.contains(valid.get(i).term) &&
			mentrez.containsKey(valid.get(i).term)){
				query.add(mentrez.get(valid.get(i).term));
				//System.out.println(valid.get(i).term + "\t" + mentrez.get(valid.get(i).term));
			}
		}
		Collections.sort(query);

		if(overlapOnly){
			String term = new String(doTerm);
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			Set<String> si = new HashSet<String>();
			for(Integer ii : overlap)
				for(String ss : meg.get(mreverse.get(ii)))
					si.add(ss);
			Vector<String> vi = new Vector(si);
			Collections.sort(vi);
			String svi = StringUtils.join(vi, " ") + "\n";
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(svi);
			System.out.println(svi);
			return ;
		}

		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new 
			HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			//System.out.println(term);
			//if(overlap.size()<=1) continue;
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new 
				HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		String st = "/tmp/" + tempFile.getName() + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
			}
		}catch(Exception e){
			System.out.println("BAD IO 1");
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		String sv = "/tmp/" + tempFile.getName() + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		DecimalFormat myFormat = new DecimalFormat("0.##E0");

		double prev_value = 0;
		//output results to client
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			double b_value = pi.val * gold_std_int.get(pi.term).size() / (double) (i+1);
			if(b_value>1){
				b_value = 1.0;
			}
			if(b_value<prev_value){
				b_value = prev_value;
			}
			prev_value = b_value;
			
			if(goldstd.equals("targetscan") || goldstd.equals("targetscan_family")){

				if(map_overlap.get(pi.term).size()<=1) continue;
				if(pi.val>=0.05 || qval.get(i)>0.5) continue;
				String decp = myFormat.format(b_value);
				String decq = myFormat.format(qval.get(i));
				res.getWriter().write(pi.term + "\t" + decp + "\t" + decq +"\t" + 
					gold_std_int.get(pi.term).size() + "\t" + query.size() + "\t" + 
					map_overlap.get(pi.term).size() + "\n");
			}else{
				if(pi.val>=0.05 || qval.get(i)>0.25){
					break;
				}
				String decp = myFormat.format(b_value);
				String decq = myFormat.format(qval.get(i));
				res.getWriter().write(pi.term + "\t" + decp + "\t" + decq +"\t" + 
					gold_std_int.get(pi.term).size() + "\t" + query.size() + "\t" + 
					map_overlap.get(pi.term).size() + "\n");
			}
			if(showGenes){
				for(Integer ii : map_overlap.get(pi.term))
					for(String ss : meg.get(mreverse.get(ii))){
						res.getWriter().write(ss + " ");
					}				
				res.getWriter().write("\n");
			}

		}

	}


}
