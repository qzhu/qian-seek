package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.Network;
import seek.ReadPacket;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class CoexpressionImage extends SeekImage{
    final static int maxCharHeight = 15;
    final static int minFontSize = 15;

    final static Color fg = Color.black;
    final static Color bg = Color.white;
    final static Color red = Color.red;
    final static Color white = Color.white;
    final static Color green = Color.green;
    final static Color black = Color.black;

    BufferedImage image;
	BufferedImage column_header_image;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = 
		new BasicStroke(1.0f, BasicStroke.CAP_BUTT, 
		BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    Dimension totalSize;
    FontMetrics fontMetrics;
    Random rnd;

	Vector<String> dataset_annot;

	float[][] comat;
    float[][] matrix;
    //float[][][] norm_matrix;
	//float[][][] tmp_norm_matrix;    

	int neutral_red = 255;
	int neutral_green = 255;
	int neutral_blue = 229;
	
	int[] pos_red = {255, 237, 199, 127, 65, 34, 37, 8};
	int[] pos_green = {255, 248, 233, 205, 182, 145, 94, 52, 29};
	int[] pos_blue = {217, 217, 180, 187, 196, 192, 168, 148, 88};
	int[] neg_red = {255, 255, 254, 254, 254, 236, 204, 153, 102};
	int[] neg_green = {255, 247, 227, 196, 153, 112, 76, 52, 37};
	int[] neg_blue = {229, 188, 145, 79, 41, 20, 2, 4, 6};

	/*
	int[] pos_red = {247, 229, 204, 153, 102, 65, 35, 0, 0};
	int[] pos_green = {252, 245, 236, 216, 194, 174, 139, 109, 68};
	int[] pos_blue = {253, 249, 230, 201, 164, 118, 69, 44, 27};
	int[] neg_red = {247, 224, 191, 158, 140, 140, 136, 129, 77};
	int[] neg_green = {252, 236, 211, 188, 150, 107, 65, 15, 0};
	int[] neg_blue = {253, 244, 230, 218, 198, 177, 157, 124, 75};
	*/

    int[][] color_red;
    int[][] color_green;
    int[][] color_blue;
    int[] size;
    
    int numDatasetToShow;
    int numGeneToShow;
    int windowWidth;
    int windowHeight;
    String[] names;
    String[] query_names;
    String[] description;
    Vector<String> dsets;
    Vector<String> genes;
	Vector<String> query;

	Vector<Float> gene_score;
	Vector<Float> dataset_score;
    Map<String, String> map_genes;
    Map<String, String> map_datasets;
	Map<String, String> map_gene_symbol_entrez;

	String dset_order_file;   
 
    //final static int horizontalDatasetSpacing = 2;
    final static int horizontalDatasetSpacing = 10;    
    final static int verticalGeneSpacing = 0;
    final static int geneNameWidth = 0;
    final static int verticalUnitHeight = 15;
	final static int width_per_dataset = 15;    	

	final static int column_header_height = 90;
	int dataset_start;
	//column header width is same as windowWidth

    private int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }

	public Color[] GetColorGradient(boolean isPositive){
		Color[] c = new Color[pos_red.length];
		if(isPositive){
			for(int i=0; i<pos_red.length; i++){
				c[i] = new Color(pos_red[i], pos_green[i], pos_blue[i]);
			}
		}else{
			for(int i=0; i<neg_red.length; i++){
				c[i] = new Color(neg_red[i], neg_green[i], neg_blue[i]);
			}
		}
		return c;
	}

	public Color GetNeutralColor(){
		return new Color(neutral_red, neutral_green, neutral_blue);
	}

	public int GetVerticalUnitHeight(){
		return verticalUnitHeight;
	}

	public int GetHorizontalUnitWidth(){
		return 2;
	}

	public int[] GetDatasetSize(){
		size = new int[dsets.size()];
		for(int i=0; i<dsets.size(); i++){
			size[i] = width_per_dataset;	
		}
		return size;
	}
    
	public int GetHorizontalDatasetSpacing(){
		return horizontalDatasetSpacing;
	}

	public int GetVerticalGeneSpacing(){
		return verticalGeneSpacing;
	}

    private int calculateUnitWidth(int s){
    	if(s<=10){
    		return 2;
    	}else if(s<=50){
    		return 2;
    	}else if(s<=200){
    		return 2;
    	}else if(s<=300){
    		return 2;
    	}else{
    		return 2;
    	}
    }

    public void calculateSize(){
    	int x = 5 + geneNameWidth;
    	int y = 0;

    	for(int i=0; i<numDatasetToShow; i++){
			x+=width_per_dataset;
    		//x+=width_per_dataset + horizontalDatasetSpacing;
    	}
    	x+=10;
    	
    	y += verticalUnitHeight*numGeneToShow;
    	//y += 30;
    	//y += numDatasetToShow * verticalUnitHeight;
    	
    	windowWidth = x;
    	windowHeight = y;
    }
    
    public int getWindowWidth(){
    	return windowWidth;
    }
    
    public int getWindowHeight(){
    	return windowHeight;
    }
    
    public void save(String image_name, String column_header_image_name){
    	File f = new File(image_name);
    	File f2 = new File(column_header_image_name);
    	try{
    		ImageIO.write(image, "png", f);
			ImageIO.write(column_header_image, "png", f2);
    	}catch(IOException e){	
    	}
    }
    
    public void init(float[][] comat) throws IOException{
        rnd = new Random();
        
        matrix = comat;
        int i, j, k;
       
		//matrix - datasets, then genes
		int num_datasets = dsets.size();
		int num_genes = genes.size();

		color_red = new int[num_genes][num_datasets];
		color_green = new int[num_genes][num_datasets];
		color_blue = new int[num_genes][num_datasets];

        names = new String[genes.size()];
        query_names = new String[query.size()];
        float clip = 10.0f;
		int pos_max_size = pos_red.length;
		int neg_max_size = neg_red.length;
		float pos_unit_length = clip / (float) pos_max_size;
		float neg_unit_length = clip / (float) neg_max_size;

		//color_red - genes, then datasets
        for(i=0; i<matrix.length; i++){ //dataset
        	for(j=0; j<matrix[i].length; j++){ //genes
        		color_red[j][i] = neutral_red;
        		color_green[j][i] = neutral_green;
        		color_blue[j][i] = neutral_blue;

				//System.out.println(i + " " + j + " " + matrix[i][j]);

        		if(matrix[i][j]>327.0f){
        			color_green[j][i] = neutral_green;
        			color_red[j][i] = neutral_red;
        			color_blue[j][i] = neutral_blue;
        			continue;
        		}

        		if(matrix[i][j]>0.0f){
        			if(matrix[i][j]>clip){
        				color_red[j][i] = pos_red[pos_max_size-1];
        				color_green[j][i] = pos_green[pos_max_size-1];
        				color_blue[j][i] = pos_blue[pos_max_size-1];
        			}else{
						int level = (int) Math.round((float)(1.0f * matrix[i][j] / pos_unit_length));
						if(level<0){
							level = 0;
						}else if(level>=pos_max_size){
							level = pos_max_size - 1;
						}
        				color_red[j][i] = pos_red[level];
        				color_green[j][i] = pos_green[level];
        				color_blue[j][i] = pos_blue[level];
        			}
        		}else if(matrix[i][j]<0.0f){
        			if(matrix[i][j]<-1.0f*clip){
        				color_red[j][i] = neg_red[neg_max_size-1];
        				color_green[j][i] = neg_green[neg_max_size-1];
        				color_blue[j][i] = neg_blue[neg_max_size-1];
        			}else{
						int level = (int) Math.round((float)(-1.0f * matrix[i][j] / neg_unit_length));
						if(level<0){
							level = 0;
						}else if(level>=neg_max_size){
							level = neg_max_size - 1;
						}
        				color_red[j][i] = neg_red[level];
        				color_green[j][i] = neg_green[level];
        				color_blue[j][i] = neg_blue[level];
        			}
        		}
        	}
        }
       
		//try{ 
        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }
        for(i=0; i<query_names.length; i++){
        	query_names[i] = new String(map_genes.get(query.get(i)));
        }
		/*}catch(Exception e){
			System.out.println("Exception: " + genes.get(i));
			if(!map_genes.containsKey(genes.get(i))){
				System.out.println("map_genes does not contain key!");
			}
		}*/
        
        int STRING_MAX_LENGTH = 200;
        description = new String[numDatasetToShow];
        
        for(k=0; k<numDatasetToShow; k++){
        	String stem = getStem(dsets.get(k));
        	String descr = map_datasets.get(stem);
        	if(descr.length()>STRING_MAX_LENGTH){
        		descr = descr.substring(0, 200) + "...";
        	}
        	description[k] = "#" + (k+1) + "  " + stem + ": " + descr;
        }
        
        /*for(i=0; i<names.length; i++){
        	char[] ca = new char[10];
        	for(j=0; j<ca.length; j++){
        		if(j%2==0){
        			int ri = rnd.nextInt(2);
        			int cc = rnd.nextInt(26);
        			if(ri==0){
        				ca[j] = 'a';
        				ca[j] += cc;
        			}else{
        				ca[j] = 'A';
        				ca[j] += cc;
        			}
        		}else{
        			int ri = rnd.nextInt(10);
        			ca[j] = '0';
        			ca[j] += ri;
        		}
        	}
        	names[i] = new String(ca);
        }*/
        
        //new
        calculateSize();
        image = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
        column_header_image = new BufferedImage(windowWidth, column_header_height, BufferedImage.TYPE_INT_RGB);
    }

    FontMetrics pickFont(Graphics2D g2,
                         String longString,
                         int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();

        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            }
            else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

    public void paint() {
        Graphics2D g2 = image.createGraphics();
		Graphics2D g3 = column_header_image.createGraphics();

        g3.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);
       // Dimension d = getSize();
        //int gridWidth = (int) (windowWidth / 30.0);
        //int gridHeight = (int) (windowHeight / 20.0);

        //fontMetrics = pickFont(g2, "Filled and Stroked GeneralPath",
        //                     gridWidth);

        Color fg3D = Color.lightGray;


        int x=5; //initial coordinate
        int y=15; //initial coordinate

        //int rectWidth = gridWidth;
        //int rectHeight = gridHeight;
        
        int i, j, k;
        //x = 5 + geneNameWidth;
        
        /*int maxDatasetWidth = 0;
        for(k=0; k<numDatasetToShow; k++){
        	int strwidth = g2.getFontMetrics().stringWidth(description[k]);
        	if( strwidth > maxDatasetWidth)
        		maxDatasetWidth = strwidth;
        }
        maxDatasetWidth+=50;
        
        int numCol = (windowWidth - geneNameWidth) / maxDatasetWidth;
        if(numCol==0) numCol = 1;
        int rowsPerCol = numDatasetToShow / numCol;
        if(rowsPerCol%numCol>0){
        	rowsPerCol++;
        }*/

		//drawing header image=================================
        g3.setPaint(bg);
        g3.fill(new Rectangle2D.Double(0, 0, windowWidth, column_header_height));
        g3.setPaint(fg);
        int kk = 0;
		y=90;
        g3.setPaint(fg);
		x = 18;
		AffineTransform original = g3.getTransform();
		for(k=0; k<dsets.size(); k++){
			g3.setTransform(original);
			g3.translate(x, 90);
			g3.rotate(-1.0*Math.toRadians(90));
			g3.setColor(Color.BLACK);
			g3.drawString((dataset_start + k+1) + ". " + 
				StringUtils.split(dataset_annot.get(k), ".")[0], 0, 0);
			x+=15;
		}
		//======================================================

		//g3.setTransform(original);
       	y=0;
        //y=initialY + rowsPerCol * verticalUnitHeight + 10;
        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, windowWidth, windowHeight));
        g2.setPaint(fg);

        int[] unitWidth = new int[dsets.size()];
        for(i=0; i<dsets.size(); i++){ //dataset
			unitWidth[i] = width_per_dataset;
        }

        /*
        x = 5 + geneNameWidth;
        for(i=0; i<numDatasetToShow; i++){
        	g2.drawString("#" + (i+1), x, y);
        	x += unitWidth[i] * size[i] + horizontalDatasetSpacing;
        }
        y+=5;
		*/

		//y = 0;        
        for(k=0; k<numGeneToShow; k++){
        	x = 5;
			//print gene name
        	//g2.setPaint(fg);
        	//g2.drawString(names[k], x, y+10);
			//System.out.println(names[k]);
        	x += geneNameWidth;
        	for(i=0; i<dsets.size(); i++){ //dataset
				//for(j=0; j<genes.size(); j++){
        		//for(j=0; j<size[i]; j++){ //column in dataset
					if(matrix[i][k]>327f){
        			//if(matrix[i][j][k]>327f){
        				//g2.setPaint(new Color(128, 128, 128));
        				g2.setPaint(new Color(neutral_red, neutral_green, neutral_blue));
        			}else{
        				g2.setPaint(new Color(
        					color_red[k][i], color_green[k][i], color_blue[k][i]));
        			}
        			g2.fill(new Rectangle2D.Double(x, y, unitWidth[i], 
        				verticalUnitHeight));
        			x+=unitWidth[i];
        		//}
        		//x+=horizontalDatasetSpacing;
        	}
        	y+=verticalUnitHeight;
			
        }
        
        
    }
   
	public String[] getGeneNames(){
		return names;
	}

	public String[] getQueryNames(){
		return query_names;
	}
 
    public String getStem(String s){
    	int i = s.indexOf("_");
    	int j = s.indexOf(".");
    	if(i!=-1){
    		return s.substring(0, i);
    	}
    	return s.substring(0, j);
    }
    
    public Vector<String> getDatasets(){
    	return dsets;
    }
    
    public Vector<String> getGenes(){
    	return genes;
    }

    public Vector<String> getQuery(){
    	return query;
    }
   
	public Vector<Float> getGeneScore(){
		return gene_score;
	}

	public Vector<Float> getDatasetScore(){
		return dataset_score;
	}
 
    public byte[] getDatasetByte() throws IOException{
		String strDatasets = StringUtils.join(dsets, "\t");
		return Network.getStringByte(strDatasets);
    }
    
    public byte[] intToByteArray(int value) {
    	ByteBuffer buf = ByteBuffer.allocate(10);
    	buf.order(ByteOrder.LITTLE_ENDIAN);
    	buf.putInt(value);
    	return new byte[] {
    		buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
    }
    
    public byte[] getGeneByte() throws IOException{
		String strGenes = StringUtils.join(genes, "\t");
		return Network.getStringByte(strGenes);
    }

	public boolean ReadMap(String gm, String dm) throws IOException{
    	Scanner sc = null;

    	map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    		//System.out.println(s2 + " " + s3);
    	}finally{
    		sc.close();
    	}

    	map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}

		return true;

	}

	//generates a coexpressed image
	public boolean read(String tempDir,
		String query_path,
		String gene_path, String dataset_path,
		int numD, int numG, 
		int dPage, int gPage) throws IOException{
    		
		Map<String, Integer> mm = null;
		int gStart, dStart;
		gStart = (gPage - 1) * numG;
		dStart = (dPage - 1) * numD;
		dataset_start = dStart;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();
		query = new Vector<String>();

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		Scanner ss = null;

		try{
			//read the query file======================
    		ss = new Scanner(new BufferedReader(new FileReader(query_path)));
    		String s1 = ss.nextLine();
    		StringTokenizer st = new StringTokenizer(s1);
			while(st.hasMoreTokens()){
				query.add(st.nextToken());
			}
			ss.close();
			//=========================================

			map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(tempDir + "/gene_entrez_symbol.txt");
    	
			name_mapping = tempDir + "/dataset_platform.jul7";
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc);

			dataset_annot = new Vector<String>();
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart + i).val;
				if(fval==0.0f){
					continue;
				}
				dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dataset_score.add(new Float(vp.get(dStart + i).val));
				dataset_annot.add(vp.get(dStart+i).term);
				System.out.println("Dataset " + vp.get(dStart + i).term + " " + fval);
			}
			
    		name_mapping = tempDir + "/gene_id.map";
    		name_mapping_2 = tempDir + "/gene_entrez_symbol.txt";
    		mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
			sc = ReadScore.ReadScoreBinary(gene_path);
			vp = ReadScore.SortScores(mm, sc);

			for(int i=0; i<numG; i++){
				genes.add(map_gene_symbol_entrez.get(vp.get(gStart + i).term));
				gene_score.add(new Float(vp.get(gStart + i).val));
				System.out.println("Gene " + map_gene_symbol_entrez.get(vp.get(gStart + i).term));
			}

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

	//	System.out.println("Query: " + genes);
    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = genes.size();
		return true;    
    }
    
    public boolean Load(String queryFile, String imageFile, 
		String column_header_file,
		int numDPerPage, int numGPerPage, int dPageNum, int gPageNum
		) 
		//boolean sortSamples, boolean useReference, boolean binaryInput) 
		throws IOException{

    	Socket kkSocket = null;
		File ff = new File(queryFile);
		String session = ff.getName();
		int session_index = session.indexOf("_");
		String sessionID = null;

		if(session_index==-1){
			sessionID = new String(session);
		}else{
			sessionID = session.substring(0, session_index);
		}

		String tempDir = ff.getParent();
 	    if(!this.read(tempDir, 
			ff.getParent() + "/" + sessionID + "_query",
			ff.getParent() + "/" + sessionID + "_gscore", 
			ff.getParent() + "/" + sessionID + "_dweight", 
			numDPerPage, numGPerPage, dPageNum, gPageNum)){
			return false;
		}

        byte[] dByte = this.getDatasetByte();
        byte[] gByte = this.getGeneByte();
		byte[] qByte = Network.getStringByte(StringUtils.join(query, "\t"));
		byte[] modeByte = Network.getStringByte("11"); //11 means coexpression=true, normalized=true

        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;

        int[] datasetSize; //don't need
        byte[] ss = null;
        ByteBuffer buf = null;
		ReadPacket st = null;
    	int num_datasets = dsets.size();
    	int num_genes = genes.size();
        float[][] comat = new float[num_datasets][num_genes];
       
    	try{
    		kkSocket = new Socket("localhost", 9001);
    		kkSocket.getOutputStream().write(modeByte, 0, modeByte.length);
    		kkSocket.getOutputStream().write(qByte, 0, qByte.length);
    		kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    		kkSocket.getOutputStream().write(dByte, 0, dByte.length);
    		
			//dataset size - don't need
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			datasetSize = new int[num_datasets];
			for(int i = 0; i<num_datasets; i++){
				datasetSize[i] = st.vecFloat.get(i).intValue();
			}

			//query expression - don't need
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			
			//gene expression - don't need
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
							
			//coexpression
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			Vector<Float> coExpr = st.vecFloat;

			int kk = 0;
			for(int i=0; i<num_datasets; i++){
				for(int g=0; g<num_genes; g++){
					float v = coExpr.get(kk);
					if(v==-9999) v = 327.67f;
					
					comat[i][g] = v * v;
					if(v<0){
						comat[i][g] *= -1.0;
					}
					kk++;
				}
			}
			
    		this.init(comat);
    		this.paint();
    		this.save(imageFile, column_header_file);
    		
    	}catch(IOException e){
    	}

		return true;
    	 //applet.init();
        //applet.paint();
        //applet.save("test.png");
    }

}
